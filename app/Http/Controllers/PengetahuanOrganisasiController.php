<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePengetahuanOrganisasiRequest;
use App\Http\Requests\UpdatePengetahuanOrganisasiRequest;
use App\Repositories\PengetahuanOrganisasiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PengetahuanOrganisasiController extends AppBaseController
{
    /** @var  PengetahuanOrganisasiRepository */
    private $pengetahuanOrganisasiRepository;

    private $dateFormat = 'd/m/Y';

    public function __construct(PengetahuanOrganisasiRepository $pengetahuanOrganisasiRepo)
    {
        $this->middleware('auth');
        $this->pengetahuanOrganisasiRepository = $pengetahuanOrganisasiRepo;
    }

    /**
     * Display a listing of the PengetahuanOrganisasi.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pengetahuanOrganisasiRepository->pushCriteria(new RequestCriteria($request));
        $pengetahuanOrganisasi = $this->pengetahuanOrganisasiRepository->all();

        return view('pengetahuan_organisasi.index')
            ->with('pengetahuanOrganisasi', $pengetahuanOrganisasi)
            ->with('dateFormat', $this->dateFormat);;
    }

    /**
     * Show the form for creating a new PengetahuanOrganisasi.
     *
     * @return Response
     */
    public function create()
    {
        return view('pengetahuan_organisasi.create')
            ->with('dateFormat', $this->dateFormat);
    }

    /**
     * Store a newly created PengetahuanOrganisasi in storage.
     *
     * @param CreatePengetahuanOrganisasiRequest $request
     *
     * @return Response
     */
    public function store(CreatePengetahuanOrganisasiRequest $request)
    {
        $input = $request->all();
        $input['tanggal'] = \Carbon\Carbon::createFromFormat($this->dateFormat, $input['tanggal']);
        $input['created_by'] = \Auth::user()->username;

        // replace with binary
        if ($request->hasFile('upload_dokumen')) {
            $path = $request->file('upload_dokumen')->path();
            $input['upload_dokumen'] = file_get_contents($path);
        }

        $pengetahuanOrganisasi = $this->pengetahuanOrganisasiRepository->create($input);
        Flash::success('Pengetahuan Organisasi saved successfully.');
        return redirect(route('pengetahuanOrganisasi.index'));
    }

    /**
     * Display the specified PengetahuanOrganisasi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pengetahuanOrganisasi = $this->pengetahuanOrganisasiRepository->findWithoutFail($id);

        if (empty($pengetahuanOrganisasi)) {
            Flash::error('Pengetahuan Organisasi not found');

            return redirect(route('pengetahuanOrganisasi.index'));
        }

        return view('pengetahuan_organisasi.show')
            ->with('pengetahuanOrganisasi', $pengetahuanOrganisasi)
            ->with('dateFormat', $this->dateFormat);
    }

    /**
     * Show the form for editing the specified PengetahuanOrganisasi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pengetahuanOrganisasi = $this->pengetahuanOrganisasiRepository->findWithoutFail($id);

        if (empty($pengetahuanOrganisasi)) {
            Flash::error('Pengetahuan Organisasi not found');

            return redirect(route('pengetahuanOrganisasi.index'));
        }

        return view('pengetahuan_organisasi.edit')
            ->with('pengetahuanOrganisasi', $pengetahuanOrganisasi)
            ->with('dateFormat', $this->dateFormat);
    }

    /**
     * Update the specified PengetahuanOrganisasi in storage.
     *
     * @param  int              $id
     * @param UpdatePengetahuanOrganisasiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePengetahuanOrganisasiRequest $request)
    {
        $pengetahuanOrganisasi = $this->pengetahuanOrganisasiRepository->findWithoutFail($id);

        if (empty($pengetahuanOrganisasi)) {
            Flash::error('Pengetahuan Organisasi not found');
            return redirect(route('pengetahuanOrganisasi.index'));
        }

        $input = $request->all();        
        $input['tanggal'] = \Carbon\Carbon::createFromFormat($this->dateFormat, $input['tanggal']);
        $input['updated_by'] = \Auth::user()->username;

        // replace with binary
        if ($request->hasFile('upload_dokumen')) {
            $path = $request->file('upload_dokumen')->path();
            $input['upload_dokumen'] = file_get_contents($path);
        }
        
        $pengetahuanOrganisasi = $this->pengetahuanOrganisasiRepository->update($input, $id);
        Flash::success('Pengetahuan Organisasi updated successfully.');
        return redirect(route('pengetahuanOrganisasi.index'));
    }

    /**
     * Remove the specified PengetahuanOrganisasi from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pengetahuanOrganisasi = $this->pengetahuanOrganisasiRepository->findWithoutFail($id);

        if (empty($pengetahuanOrganisasi)) {
            Flash::error('Pengetahuan Organisasi not found');

            return redirect(route('pengetahuanOrganisasi.index'));
        }

        $this->pengetahuanOrganisasiRepository->delete($id);

        Flash::success('Pengetahuan Organisasi deleted successfully.');

        return redirect(route('pengetahuanOrganisasi.index'));
    }
}
