<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTupoksiJabatanRequest;
use App\Http\Requests\UpdateTupoksiJabatanRequest;
use App\Repositories\TupoksiJabatanRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TupoksiJabatanController extends AppBaseController
{
    /** @var  TupoksiJabatanRepository */
    private $tupoksiJabatanRepository;

    public function __construct(TupoksiJabatanRepository $tupoksiJabatanRepo)
    {
        $this->middleware('auth');
        $this->tupoksiJabatanRepository = $tupoksiJabatanRepo;
    }

    /**
     * Display a listing of the TupoksiJabatan.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tupoksiJabatanRepository->pushCriteria(new RequestCriteria($request));
        $tupoksiJabatans = $this->tupoksiJabatanRepository->all();

        return view('tupoksi_jabatan.index')
            ->with('tupoksiJabatans', $tupoksiJabatans);
    }

    /**
     * Show the form for creating a new TupoksiJabatan.
     *
     * @return Response
     */
    public function create()
    {
        return view('tupoksi_jabatan.create');
    }

    /**
     * Store a newly created TupoksiJabatan in storage.
     *
     * @param CreateTupoksiJabatanRequest $request
     *
     * @return Response
     */
    public function store(CreateTupoksiJabatanRequest $request)
    {
        
        $input = $request->all();

        // replace with binary
        if ($request->hasFile('upload_dokumen')) {
            $path = $request->file('upload_dokumen')->path();
            $input['upload_dokumen'] = file_get_contents($path);
        }

        $tupoksiJabatan = $this->tupoksiJabatanRepository->create($input);

        Flash::success('Tupoksi Jabatan saved successfully.');

        return redirect(route('tupoksiJabatan.index'));
    }

    /**
     * Display the specified TupoksiJabatan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tupoksiJabatan = $this->tupoksiJabatanRepository->findWithoutFail($id);

        if (empty($tupoksiJabatan)) {
            Flash::error('Tupoksi Jabatan not found');

            return redirect(route('tupoksiJabatan.index'));
        }

        return view('tupoksi_jabatan.show')->with('tupoksiJabatan', $tupoksiJabatan);
    }

    /**
     * Show the form for editing the specified TupoksiJabatan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tupoksiJabatan = $this->tupoksiJabatanRepository->findWithoutFail($id);

        if (empty($tupoksiJabatan)) {
            Flash::error('Tupoksi Jabatan not found');

            return redirect(route('tupoksiJabatan.index'));
        }

        return view('tupoksi_jabatan.edit')->with('tupoksiJabatan', $tupoksiJabatan);
    }

    /**
     * Update the specified TupoksiJabatan in storage.
     *
     * @param  int              $id
     * @param UpdateTupoksiJabatanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTupoksiJabatanRequest $request)
    {
        $tupoksiJabatan = $this->tupoksiJabatanRepository->findWithoutFail($id);

        if (empty($tupoksiJabatan)) {
            Flash::error('Tupoksi Jabatan not found');

            return redirect(route('tupoksiJabatan.index'));
        }        

        $input = $request->all();

        // replace with binary
        if ($request->hasFile('upload_dokumen')) {
            $path = $request->file('upload_dokumen')->path();
            $input['upload_dokumen'] = file_get_contents($path);
        }

        $tupoksiJabatan = $this->tupoksiJabatanRepository->update($input, $id);

        Flash::success('Tupoksi Jabatan updated successfully.');

        return redirect(route('tupoksiJabatan.index'));
    }

    /**
     * Remove the specified TupoksiJabatan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tupoksiJabatan = $this->tupoksiJabatanRepository->findWithoutFail($id);

        if (empty($tupoksiJabatan)) {
            Flash::error('Tupoksi Jabatan not found');

            return redirect(route('tupoksiJabatan.index'));
        }

        $this->tupoksiJabatanRepository->delete($id);

        Flash::success('Tupoksi Jabatan deleted successfully.');

        return redirect(route('tupoksiJabatan.index'));
    }
}
