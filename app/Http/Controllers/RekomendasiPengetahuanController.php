<?php

namespace App\Http\Controllers;

use InfyOm\Generator\Common\BaseRepository;
use App\Repositories\PeraturanPemerintahRepository;
use App\Repositories\SumberPengetahuanBaruRepository;
use App\Repositories\KasusRepository;
use App\Repositories\ExpertRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;


class RekomendasiPengetahuanController extends AppBaseController
{

    /** @var  PeraturanPemerintahRepository */
    private $peraturanPemerintahRepository;

    private $dateFormat = 'd/m/Y';

    public function __construct(
        PeraturanPemerintahRepository $peraturanPemerintahRepo,
        SumberPengetahuanBaruRepository $sumberPengetahuanBaruRepo,
        KasusRepository $kasusRepo,
        ExpertRepository $expertRepo
    )
    {
        $this->middleware('auth');
        $this->peraturanPemerintahRepository = $peraturanPemerintahRepo;
        $this->sumberPengetahuanBaruRepository = $sumberPengetahuanBaruRepo;
        $this->kasusRepository = $kasusRepo;
        $this->expertRepository = $expertRepo;
    }

    /**
     * Return data from given repository
     * filtered by PengetahuanOrganisasi with highest KnowledgeGap->gap
     * gap = kepentingan - pemahaman
     *  
     */    
    private function getFilteredData(Request $request, BaseRepository $repository, String $relationName)
    {        
        $repository->pushCriteria(new RequestCriteria($request));
        $list = $repository->all();

        if (\Auth::user()->role == \App\User::ROLES['PEGAWAI']) {
            // find max gap
            // TODO: find faster way
            $knowlegeGaps = \App\Models\KnowledgeGap::where('pegawai_id', \Auth::user()->pegawai->id)->get();
            $knowlegeGap = null;
            foreach ($knowlegeGaps as $kg) {
                if ($knowlegeGap == null) {
                    $knowlegeGap = $kg;
                } else {
                    if ($kg->gap > $knowlegeGap->gap) {
                        $knowlegeGap = $kg;
                    }
                }
            }

            $list = $knowlegeGap->pengetahuanOrganisasi->{$relationName};
        }

        return $list;
    }


    /**
     * Display a listing of the PeraturanPemerintah.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->peraturanPemerintah($request);   
    }


    /**
     * Display a listing of the PeraturanPemerintah
     *
     * @param Request $request
     * @return Response
     */
    public function peraturanPemerintah(Request $request)
    {        
        $peraturanPemerintah = $this->getFilteredData(
            $request,
            $this->peraturanPemerintahRepository,
            'peraturanPemerintah'
        );

        return view('rekomendasi_pengetahuan.peraturan_pemerintah.index')
            ->with('peraturanPemerintah', $peraturanPemerintah)
            ->with('dateFormat', $this->dateFormat);
    }


    /**
     * Display a listing of the SumberPengetahuanBaru
     *
     * @param Request $request
     * @return Response
     */
    public function sumberPengetahuanBaru(Request $request)
    {        
        $sumberPengetahuanBaru = $this->getFilteredData(
            $request,
            $this->sumberPengetahuanBaruRepository,
            'sumberPengetahuanBaru'
        );

        return view('rekomendasi_pengetahuan.sumber_pengetahuan_baru.index')
            ->with('sumberPengetahuanBaru', $sumberPengetahuanBaru)
            ->with('dateFormat', $this->dateFormat);
    }


    /**
     * Display a listing of the Kasus
     *
     * @param Request $request
     * @return Response
     */
    public function kasus(Request $request)
    {        
        $kasus = $this->getFilteredData(
            $request,
            $this->kasusRepository,
            'kasus'
        );

        return view('rekomendasi_pengetahuan.kasus.index')
            ->with('kasus', $kasus)
            ->with('dateFormat', $this->dateFormat);
    }

    
    /**
     * Display a listing of the Expert
     *
     * @param Request $request
     * @return Response
     */
    public function experts(Request $request)
    {        
        $experts = $this->getFilteredData(
            $request,
            $this->expertRepository,
            'experts'
        );

        return view('rekomendasi_pengetahuan.experts.index')
            ->with('experts', $experts)
            ->with('dateFormat', $this->dateFormat);
    }
    
}
