<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSumberPengetahuanBaruRequest;
use App\Http\Requests\UpdateSumberPengetahuanBaruRequest;
use App\Repositories\SumberPengetahuanBaruRepository;
use App\Repositories\UserRepository;
use App\Repositories\PengetahuanOrganisasiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SumberPengetahuanBaruController extends AppBaseController
{
    /** @var  SumberPengetahuanBaruRepository */
    private $sumberPengetahuanBaruRepository;

    /** @var  UserRepository */
    private $userRepository;

    /** @var  PengetahuanOrganisasiRepository */
    private $pengetahuanOrganisasiRepository;    
    
    public function __construct (
        SumberPengetahuanBaruRepository $sumberPengetahuanBaruRepo,
        UserRepository $userRepo,
        PengetahuanOrganisasiRepository $pengetahuanOrganisasiRepo    )
    {
        $this->middleware('auth');
        $this->sumberPengetahuanBaruRepository = $sumberPengetahuanBaruRepo;
        $this->userRepository = $userRepo;
        $this->pengetahuanOrganisasiRepository = $pengetahuanOrganisasiRepo;        
    }

    /**
     * Display a listing of the SumberPengetahuanBaru.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sumberPengetahuanBaruRepository->pushCriteria(new RequestCriteria($request));
        $sumberPengetahuanBaru = $this->sumberPengetahuanBaruRepository->all();

        return view('sumber_pengetahuan_baru.index')
            ->with('sumberPengetahuanBaru', $sumberPengetahuanBaru);
    }

    /**
     * Show the form for creating a new SumberPengetahuanBaru.
     *
     * @return Response
     */
    public function create()
    {
        $userList = $this->userRepository->orderBy('name', 'asc')->pluck('name', 'id')->all();
        $pengetahuanList = $this->pengetahuanOrganisasiRepository->orderBy('nama_pengetahuan', 'asc')->pluck('nama_pengetahuan', 'id')->all();        
        return view('sumber_pengetahuan_baru.create')
            ->with('userList', $userList)
            ->with('pengetahuanList', $pengetahuanList);
    }

    /**
     * Store a newly created SumberPengetahuanBaru in storage.
     *
     * @param CreateSumberPengetahuanBaruRequest $request
     *
     * @return Response
     */
    public function store(CreateSumberPengetahuanBaruRequest $request)
    {
        $input = $request->all();

        // replace with binary
        $input = $this->replaceInputFiles([
            'upload_foto',
            'upload_dokumen',
            'upload_video',
            'upload_audio',
        ], $input, $request);
        
        $input['created_by'] = \Auth::user()->username;
        
        $sumberPengetahuanBaru = $this->sumberPengetahuanBaruRepository->create($input);

        Flash::success('Sumber Pengetahuan Baru saved successfully.');

        return redirect(route('sumberPengetahuanBaru.index'));
    }

    /**
     * Display the specified SumberPengetahuanBaru.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sumberPengetahuanBaru = $this->sumberPengetahuanBaruRepository->with(['user', 'pengetahuanOrganisasi'])->findWithoutFail($id);

        if (empty($sumberPengetahuanBaru)) {
            Flash::error('Sumber Pengetahuan Baru not found');

            return redirect(route('sumberPengetahuanBaru.index'));
        }

        return view('sumber_pengetahuan_baru.show')->with('sumberPengetahuanBaru', $sumberPengetahuanBaru);
    }

    /**
     * Show the form for editing the specified SumberPengetahuanBaru.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sumberPengetahuanBaru = $this->sumberPengetahuanBaruRepository->findWithoutFail($id);

        if (empty($sumberPengetahuanBaru)) {
            Flash::error('Sumber Pengetahuan Baru not found');

            return redirect(route('sumberPengetahuanBaru.index'));
        }

        $userList = $this->userRepository->orderBy('name', 'asc')->pluck('name', 'id')->all();
        $pengetahuanList = $this->pengetahuanOrganisasiRepository->orderBy('nama_pengetahuan', 'asc')->pluck('nama_pengetahuan', 'id')->all();        
        return view('sumber_pengetahuan_baru.edit')
            ->with('sumberPengetahuanBaru', $sumberPengetahuanBaru)
            ->with('userList', $userList)
            ->with('pengetahuanList', $pengetahuanList);
    }

    /**
     * Update the specified SumberPengetahuanBaru in storage.
     *
     * @param  int              $id
     * @param UpdateSumberPengetahuanBaruRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSumberPengetahuanBaruRequest $request)
    {
        $sumberPengetahuanBaru = $this->sumberPengetahuanBaruRepository->findWithoutFail($id);

        if (empty($sumberPengetahuanBaru)) {
            Flash::error('Sumber Pengetahuan Baru not found');

            return redirect(route('sumberPengetahuanBaru.index'));
        }

        $input = $request->all();

        // replace with binary
        $input = $this->replaceInputFiles([
            'upload_foto',
            'upload_dokumen',
            'upload_video',
            'upload_audio',
        ], $input, $request);

        $input['updated_by'] = \Auth::user()->username;
        
        $sumberPengetahuanBaru = $this->sumberPengetahuanBaruRepository->update($input, $id);

        Flash::success('Sumber Pengetahuan Baru updated successfully.');

        return redirect(route('sumberPengetahuanBaru.index'));
    }

    /**
     * Remove the specified SumberPengetahuanBaru from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sumberPengetahuanBaru = $this->sumberPengetahuanBaruRepository->findWithoutFail($id);

        if (empty($sumberPengetahuanBaru)) {
            Flash::error('Sumber Pengetahuan Baru not found');

            return redirect(route('sumberPengetahuanBaru.index'));
        }

        $this->sumberPengetahuanBaruRepository->delete($id);

        Flash::success('Sumber Pengetahuan Baru deleted successfully.');

        return redirect(route('sumberPengetahuanBaru.index'));
    }
}
