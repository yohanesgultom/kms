<?php

namespace App\Http\Controllers;

use App\Models\ChatSession;
use App\Models\ChatMessage;
use App\Models\ChatSessionUser;
use App\Repositories\ChatSessionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

/**
 * Class ChatSessionController
 * @package App\Http\Controllers\API
 */

class ChatSessionController extends AppBaseController
{
    /** @var  ChatSessionRepository */
    private $chatSessionRepository;

    public function __construct(ChatSessionRepository $chatSessionRepo)
    {
        $this->middleware('auth');
        $this->chatSessionRepository = $chatSessionRepo;
    }

    public function start(Request $request) {
        $input = $request->all();
        for ($i = 0; $i < count($input['user_id']); $i++) {
             $input['user_id'][$i] = (int) $input['user_id'][$i];
        }
        $userIds = array_merge([\Auth::id()], $input['user_id']);
        $chatSession = $this->chatSessionRepository->findOrCreate($userIds);
        $recipients = [];
        foreach ($chatSession->users as $user) {
            if ($user->id != \Auth::id()) {
                array_push($recipients, $user);
            }
        }

        return $this->sendResponse([
            'sender' => \Auth::user(),
            'session' => $chatSession,
            'recipients' => $recipients,
            'messages' => $chatSession->messages
        ], null);
    }

    public function send($id, Request $request) {        
        $chatSession = $this->chatSessionRepository->findWithoutFail($id);
        if (empty($chatSession)) {
            return $this->response('Chat Session not found', 404);
        }
        $input = $request->all();
        $chatMessage = $chatSession->messages()->create([
            'user_id' => \Auth::id(),
            'message' => $input['message']
        ]);
        // update number of unread messages
        $affected = DB::update('UPDATE chat_session_user SET message_count = message_count + 1 WHERE chat_session_id = ? AND user_id != ?', [$id, \Auth::id()]);

        return $this->sendResponse($chatMessage, 'Chat Message sent');
    }

    public function sync($id, Request $request) {
        $input = $request->all();
        if (empty($input['last_message_id'])) {
            return $this->response('last_message_id is required', 400);
        }
        $newMessages = ChatMessage::with('user')
            ->where('chat_session_id', $id)
            ->where('id', '>', $input['last_message_id'])
            ->get();

        // reset number of unread messages
        if (count($newMessages) > 0) {
            $affected = DB::update('UPDATE chat_session_user SET message_count = 0 WHERE chat_session_id = ? AND user_id = ?', [$id, \Auth::id()]);
        }
            
        return $this->sendResponse($newMessages, null);
    }    


    public function count(Request $request) {
        $sessionUsers = ChatSessionUser::with(['user', 'chat.users'])->where('user_id', \Auth::id())->get();
        return $this->sendResponse($sessionUsers, null);
    }

    public function load($id, Request $request) {
        $chatSession = $this->chatSessionRepository->findWithoutFail($id);
        if (empty($chatSession)) {
            return $this->response('Chat Session not found', 404);
        }
        $recipients = [];
        foreach ($chatSession->users as $user) {
            if ($user->id != \Auth::id()) {
                array_push($recipients, $user);
            }
        }
        // reset number of unread messages
        $affected = DB::update('UPDATE chat_session_user SET message_count = 0 WHERE chat_session_id = ? AND user_id = ?', [$id, \Auth::id()]);

        return $this->sendResponse([
            'sender' => \Auth::user(),
            'session' => $chatSession,
            'recipients' => $recipients,
            'messages' => $chatSession->messages
        ], null);
    }
}
