<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePeraturanPemerintahRequest;
use App\Http\Requests\UpdatePeraturanPemerintahRequest;
use App\Repositories\PeraturanPemerintahRepository;
use App\Repositories\PengetahuanOrganisasiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PeraturanPemerintahController extends AppBaseController
{
    /** @var  PeraturanPemerintahRepository */
    private $peraturanPemerintahRepository;

    /** @var  PengetahuanOrganisasiRepository */
    private $pengetahuanOrganisasiRepository;

    private $dateFormat = 'd/m/Y';

    public function __construct(PeraturanPemerintahRepository $peraturanPemerintahRepo, PengetahuanOrganisasiRepository $pengetahuanOrganisasiRepository)
    {
        $this->middleware('auth');
        $this->peraturanPemerintahRepository = $peraturanPemerintahRepo;
        $this->pengetahuanOrganisasiRepository = $pengetahuanOrganisasiRepository;
    }

    /**
     * Display a listing of the PeraturanPemerintah.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->peraturanPemerintahRepository->pushCriteria(new RequestCriteria($request));
        $peraturanPemerintah = $this->peraturanPemerintahRepository->all();

        return view('peraturan_pemerintah.index')
            ->with('peraturanPemerintah', $peraturanPemerintah)
            ->with('dateFormat', $this->dateFormat);
    }

    /**
     * Show the form for creating a new PeraturanPemerintah.
     *
     * @return Response
     */
    public function create()
    {
        $pengetahuanList = $this->pengetahuanOrganisasiRepository->pluck('nama_pengetahuan', 'id')->all();
        return view('peraturan_pemerintah.create')
            ->with('pengetahuanList', $pengetahuanList)
            ->with('dateFormat', $this->dateFormat);
    }

    /**
     * Store a newly created PeraturanPemerintah in storage.
     *
     * @param CreatePeraturanPemerintahRequest $request
     *
     * @return Response
     */
    public function store(CreatePeraturanPemerintahRequest $request)
    {
        $input = $request->all();
        $input['tanggal'] = \Carbon\Carbon::createFromFormat($this->dateFormat, $input['tanggal']);
        $input['created_by'] = \Auth::user()->username;

        // replace with binary
        if ($request->hasFile('upload_dokumen')) {
            $path = $request->file('upload_dokumen')->path();
            $input['upload_dokumen'] = file_get_contents($path);
        }

        $peraturanPemerintah = $this->peraturanPemerintahRepository->create($input);
        Flash::success('Peraturan Pemerintah saved successfully.');
        return redirect(route('peraturanPemerintah.index'));
    }

    /**
     * Display the specified PeraturanPemerintah.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $peraturanPemerintah = $this->peraturanPemerintahRepository->with('pengetahuanOrganisasi')->findWithoutFail($id);

        if (empty($peraturanPemerintah)) {
            Flash::error('Peraturan Pemerintah not found');
            return redirect(route('peraturanPemerintah.index'));
        }

        return view('peraturan_pemerintah.show')->with('peraturanPemerintah', $peraturanPemerintah)->with('dateFormat', $this->dateFormat);
    }

    /**
     * Show the form for editing the specified PeraturanPemerintah.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $peraturanPemerintah = $this->peraturanPemerintahRepository->findWithoutFail($id);

        if (empty($peraturanPemerintah)) {
            Flash::error('Peraturan Pemerintah not found');
            return redirect(route('peraturanPemerintah.index'));
        }

        $pengetahuanList = $this->pengetahuanOrganisasiRepository->pluck('nama_pengetahuan', 'id')->all();

        return view('peraturan_pemerintah.edit')
            ->with('peraturanPemerintah', $peraturanPemerintah)
            ->with('pengetahuanList', $pengetahuanList)
            ->with('dateFormat', $this->dateFormat);
    }

    /**
     * Update the specified PeraturanPemerintah in storage.
     *
     * @param  int              $id
     * @param UpdatePeraturanPemerintahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePeraturanPemerintahRequest $request)
    {
        $peraturanPemerintah = $this->peraturanPemerintahRepository->findWithoutFail($id);

        if (empty($peraturanPemerintah)) {
            Flash::error('Peraturan Pemerintah not found');
            return redirect(route('peraturanPemerintah.index'));
        }

        $input = $request->all();        
        $input['tanggal'] = \Carbon\Carbon::createFromFormat($this->dateFormat, $input['tanggal']);
        $input['updated_by'] = \Auth::user()->username;

        // replace with binary
        if ($request->hasFile('upload_dokumen')) {
            $path = $request->file('upload_dokumen')->path();
            $input['upload_dokumen'] = file_get_contents($path);
        }
        
        $peraturanPemerintah = $this->peraturanPemerintahRepository->update($input, $id);
        Flash::success('Peraturan Pemerintah updated successfully.');
        return redirect(route('peraturanPemerintah.index'));
    }

    /**
     * Remove the specified PeraturanPemerintah from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $peraturanPemerintah = $this->peraturanPemerintahRepository->findWithoutFail($id);

        if (empty($peraturanPemerintah)) {
            Flash::error('Peraturan Pemerintah not found');

            return redirect(route('peraturanPemerintah.index'));
        }

        $this->peraturanPemerintahRepository->delete($id);

        Flash::success('Peraturan Pemerintah deleted successfully.');

        return redirect(route('peraturanPemerintah.index'));
    }
}
