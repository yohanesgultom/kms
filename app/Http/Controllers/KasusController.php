<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKasusRequest;
use App\Http\Requests\UpdateKasusRequest;
use App\Repositories\KasusRepository;
use App\Repositories\UserRepository;
use App\Repositories\PengetahuanOrganisasiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class KasusController extends AppBaseController
{
    /** @var  KasusRepository */
    private $kasusRepository;

    /** @var  UserRepository */
    private $userRepository;

    /** @var  PengetahuanOrganisasiRepository */
    private $pengetahuanOrganisasiRepository;    
    
    public function __construct(
        KasusRepository $kasusRepo,
        UserRepository $userRepo,
        PengetahuanOrganisasiRepository $pengetahuanOrganisasiRepo
    )
    {
        $this->middleware('auth');
        $this->kasusRepository = $kasusRepo;
        $this->userRepository = $userRepo;
        $this->pengetahuanOrganisasiRepository = $pengetahuanOrganisasiRepo;        
    }

    /**
     * Display a listing of the Kasus.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->kasusRepository->pushCriteria(new RequestCriteria($request));
        $kasus = $this->kasusRepository->all();

        return view('kasus.index')
            ->with('kasus', $kasus);
    }

    /**
     * Show the form for creating a new Kasus.
     *
     * @return Response
     */
    public function create()
    {
        $userList = $this->userRepository->orderBy('name', 'asc')->pluck('name', 'id')->all();
        $pengetahuanList = $this->pengetahuanOrganisasiRepository->orderBy('nama_pengetahuan', 'asc')->pluck('nama_pengetahuan', 'id')->all();
        return view('kasus.create')
            ->with('userList', $userList)
            ->with('pengetahuanList', $pengetahuanList);
    }

    /**
     * Store a newly created Kasus in storage.
     *
     * @param CreateKasusRequest $request
     *
     * @return Response
     */
    public function store(CreateKasusRequest $request)
    {
        $input = $request->all();

        // replace with binary
        $input = $this->replaceInputFiles([
            'upload_foto',
            'upload_dokumen',
            'upload_video',
            'upload_audio',
        ], $input, $request);

        $input['created_by'] = \Auth::user()->username;
        
        $kasus = $this->kasusRepository->create($input);

        Flash::success('Kasus saved successfully.');

        return redirect(route('kasus.index'));
    }

    /**
     * Display the specified Kasus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $kasus = $this->kasusRepository->with(['user', 'pengetahuanOrganisasi'])->findWithoutFail($id);

        if (empty($kasus)) {
            Flash::error('Kasus not found');

            return redirect(route('kasus.index'));
        }

        return view('kasus.show')->with('kasus', $kasus);
    }

    /**
     * Show the form for editing the specified Kasus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $kasus = $this->kasusRepository->findWithoutFail($id);

        if (empty($kasus)) {
            Flash::error('Kasus not found');

            return redirect(route('kasus.index'));
        }

        $userList = $this->userRepository->orderBy('name', 'asc')->pluck('name', 'id')->all();
        $pengetahuanList = $this->pengetahuanOrganisasiRepository->orderBy('nama_pengetahuan', 'asc')->pluck('nama_pengetahuan', 'id')->all();
        return view('kasus.edit')
            ->with('kasus', $kasus)
            ->with('userList', $userList)
            ->with('pengetahuanList', $pengetahuanList);
    }

    /**
     * Update the specified Kasus in storage.
     *
     * @param  int $id
     * @param UpdateKasusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKasusRequest $request)
    {
        $kasus = $this->kasusRepository->findWithoutFail($id);

        if (empty($kasus)) {
            Flash::error('Kasus not found');

            return redirect(route('kasus.index'));
        }

        $input = $request->all();

        // replace with binary
        $input = $this->replaceInputFiles([
            'upload_foto',
            'upload_dokumen',
            'upload_video',
            'upload_audio',
        ], $input, $request);

        $input['updated_by'] = \Auth::user()->username;

        $kasus = $this->kasusRepository->update($input, $id);

        Flash::success('Kasus updated successfully.');

        return redirect(route('kasus.index'));
    }

    /**
     * Remove the specified Kasus from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $kasus = $this->kasusRepository->findWithoutFail($id);

        if (empty($kasus)) {
            Flash::error('Kasus not found');

            return redirect(route('kasus.index'));
        }

        $this->kasusRepository->delete($id);

        Flash::success('Kasus deleted successfully.');

        return redirect(route('kasus.index'));
    }
}
