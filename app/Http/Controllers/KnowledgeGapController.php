<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKnowledgeGapRequest;
use App\Http\Requests\UpdateKnowledgeGapRequest;
use App\Repositories\KnowledgeGapRepository;
use App\Repositories\PegawaiRepository;
use App\Repositories\PengetahuanOrganisasiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class KnowledgeGapController extends AppBaseController
{
    /** @var  KnowledgeGapRepository */
    private $knowledgeGapRepository;

    /** @var  PegawaiRepository */
    private $pegawaiRepository;
    
    /** @var  PengetahuanOrganisasiRepository */
    private $pengetahuanOrganisasiRepository;    

    public function __construct(
        KnowledgeGapRepository $knowledgeGapRepo,
        PegawaiRepository $pegawaiRepo,
        PengetahuanOrganisasiRepository $pengetahuanOrganisasiRepo
    )
    {
        $this->middleware('auth');
        $this->knowledgeGapRepository = $knowledgeGapRepo;
        $this->pegawaiRepository = $pegawaiRepo;
        $this->pengetahuanOrganisasiRepository = $pengetahuanOrganisasiRepo;
    }

    /**
     * Display a listing of the KnowledgeGap.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->knowledgeGapRepository->pushCriteria(new RequestCriteria($request));
        $knowledgeGaps = $this->knowledgeGapRepository->with(['pegawai.user', 'pengetahuanOrganisasi'])->all();

        return view('knowledge_gaps.index')
            ->with('knowledgeGaps', $knowledgeGaps);
    }

    /**
     * Show the form for creating a new KnowledgeGap.
     *
     * @return Response
     */
    public function create()
    {
        $pegawaiList = $this
            ->pegawaiRepository
            ->with(['user'])
            ->all()
            ->mapWithKeys(function ($item) {
                return [$item->id => $item->user->name];
            });
        $pengetahuanList = $this->pengetahuanOrganisasiRepository->pluck('nama_pengetahuan', 'id')->all();
        return view('knowledge_gaps.create')
            ->with('pegawaiList', $pegawaiList)
            ->with('pengetahuanList', $pengetahuanList);
    }

    /**
     * Store a newly created KnowledgeGap in storage.
     *
     * @param CreateKnowledgeGapRequest $request
     *
     * @return Response
     */
    public function store(CreateKnowledgeGapRequest $request)
    {
        $input = $request->all();

        $knowledgeGap = $this->knowledgeGapRepository->create($input);

        Flash::success('Knowledge Gap saved successfully.');

        return redirect(route('knowledgeGaps.index'));
    }

    /**
     * Display the specified KnowledgeGap.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $knowledgeGap = $this->knowledgeGapRepository->findWithoutFail($id);

        if (empty($knowledgeGap)) {
            Flash::error('Knowledge Gap not found');

            return redirect(route('knowledgeGaps.index'));
        }

        return view('knowledge_gaps.show')->with('knowledgeGap', $knowledgeGap);
    }

    /**
     * Show the form for editing the specified KnowledgeGap.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $knowledgeGap = $this->knowledgeGapRepository->findWithoutFail($id);

        if (empty($knowledgeGap)) {
            Flash::error('Knowledge Gap not found');

            return redirect(route('knowledgeGaps.index'));
        }

        $pegawaiList = $this
            ->pegawaiRepository
            ->with(['user'])
            ->all()
            ->mapWithKeys(function ($item) {
                return [$item->id => $item->user->name];
            });
        $pengetahuanList = $this->pengetahuanOrganisasiRepository->pluck('nama_pengetahuan', 'id')->all();        
        return view('knowledge_gaps.edit')
            ->with('knowledgeGap', $knowledgeGap)
            ->with('pegawaiList', $pegawaiList)
            ->with('pengetahuanList', $pengetahuanList);
    }

    /**
     * Update the specified KnowledgeGap in storage.
     *
     * @param  int              $id
     * @param UpdateKnowledgeGapRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKnowledgeGapRequest $request)
    {
        $knowledgeGap = $this->knowledgeGapRepository->findWithoutFail($id);

        if (empty($knowledgeGap)) {
            Flash::error('Knowledge Gap not found');

            return redirect(route('knowledgeGaps.index'));
        }

        $knowledgeGap = $this->knowledgeGapRepository->update($request->all(), $id);

        Flash::success('Knowledge Gap updated successfully.');

        return redirect(route('knowledgeGaps.index'));
    }

    /**
     * Remove the specified KnowledgeGap from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $knowledgeGap = $this->knowledgeGapRepository->findWithoutFail($id);

        if (empty($knowledgeGap)) {
            Flash::error('Knowledge Gap not found');

            return redirect(route('knowledgeGaps.index'));
        }

        $this->knowledgeGapRepository->delete($id);

        Flash::success('Knowledge Gap deleted successfully.');

        return redirect(route('knowledgeGaps.index'));
    }
}
