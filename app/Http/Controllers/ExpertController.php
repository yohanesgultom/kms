<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateExpertRequest;
use App\Http\Requests\UpdateExpertRequest;
use App\Repositories\ExpertRepository;
use App\Repositories\PengetahuanOrganisasiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ExpertController extends AppBaseController
{
    /** @var  ExpertRepository */
    private $expertRepository;

    /** @var  PengetahuanOrganisasiRepository */
    private $pengetahuanOrganisasiRepository;

    // view date format
    private $dateFormat = 'd/m/Y H:i';

    public function __construct(
        ExpertRepository $expertRepo,
        PengetahuanOrganisasiRepository $pengetahuanOrganisasiRepo
    )
    {
        $this->middleware('auth');
        $this->expertRepository = $expertRepo;
        $this->pengetahuanOrganisasiRepository = $pengetahuanOrganisasiRepo;
    }

    /**
     * Display a listing of the Expert.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->expertRepository->pushCriteria(new RequestCriteria($request));
        $experts = $this->expertRepository->with(['user'])->all();

        return view('experts.index')
            ->with('experts', $experts)
            ->with('dateFormat', $this->dateFormat);
    }

    /**
     * Show the form for creating a new Expert.
     *
     * @return Response
     */
    public function create()
    {
        $pengetahuanList = $this->pengetahuanOrganisasiRepository->pluck('nama_pengetahuan', 'id')->all();
        return view('experts.create')
            ->with('pengetahuanList', $pengetahuanList)
            ->with('dateFormat', $this->dateFormat);
    }

    /**
     * Store a newly created Expert in storage.
     *
     * @param CreateExpertRequest $request
     *
     * @return Response
     */
    public function store(CreateExpertRequest $request)
    {
        $input = $request->all();

        $input['available_date'] = \Carbon\Carbon::createFromFormat($this->dateFormat, $input['available_date']);
        $input['available_date']->setTimeZone('UTC');

        $expert = $this->expertRepository->create($input);

        Flash::success('Expert saved successfully.');

        return redirect(route('experts.index'));
    }

    /**
     * Display the specified Expert.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $expert = $this->expertRepository->with('pengetahuanOrganisasi')->with(['user'])->findWithoutFail($id);

        if (empty($expert)) {
            Flash::error('Expert not found');

            return redirect(route('experts.index'));
        }

        return view('experts.show')->with('expert', $expert)->with('dateFormat', $this->dateFormat);
    }

    /**
     * Show the form for editing the specified Expert.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $expert = $this->expertRepository->with(['user'])->findWithoutFail($id);

        if (empty($expert)) {
            Flash::error('Expert not found');

            return redirect(route('experts.index'));
        }

        $pengetahuanList = $this->pengetahuanOrganisasiRepository->pluck('nama_pengetahuan', 'id')->all();
        
        return view('experts.edit')
            ->with('expert', $expert)
            ->with('pengetahuanList', $pengetahuanList)
            ->with('dateFormat', $this->dateFormat);
    }

    /**
     * Update the specified Expert in storage.
     *
     * @param  int              $id
     * @param UpdateExpertRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateExpertRequest $request)
    {
        $expert = $this->expertRepository->with(['user'])->findWithoutFail($id);

        if (empty($expert)) {
            Flash::error('Expert not found');

            return redirect(route('experts.index'));
        }

        $input = $request->all();

        $input['available_date'] = \Carbon\Carbon::createFromFormat($this->dateFormat, $input['available_date']);
        $input['available_date']->setTimeZone('UTC');

        $expert = $this->expertRepository->update($input, $expert);

        Flash::success('Expert updated successfully.');

        return redirect(route('experts.index'));
    }

    /**
     * Remove the specified Expert from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $expert = $this->expertRepository->findWithoutFail($id);

        if (empty($expert)) {
            Flash::error('Expert not found');

            return redirect(route('experts.index'));
        }

        $this->expertRepository->delete($id);

        Flash::success('Expert deleted successfully.');

        return redirect(route('experts.index'));
    }
}
