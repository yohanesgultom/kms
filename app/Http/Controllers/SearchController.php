<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;

class SearchController extends AppBaseController
{

    public function index(Request $request)
    {
        $input = $request->all();
        return view('search')->with('query', $input['query']);
    }

    private function search($query, $tableName, $idField, $valField)
    {        
        $sql = "SELECT ${idField}, ${valField} FROM ${tableName} WHERE LOWER(${valField}) LIKE ?";
        $params = [ '%'.strtolower($query).'%' ];
        $results = DB::select($sql, $params);
        return $this->sendResponse($results, null);
    }

    public function kasus(Request $request)
    {        
        return $this->search($request->input('query'), 'kasus', 'id', 'kasus');
    }

    public function pengetahuanOrganisasi(Request $request)
    {        
        return $this->search($request->input('query'), 'pengetahuan_organisasi', 'id', 'nama_pengetahuan');
    }

    public function peraturanPemerintah(Request $request)
    {        
        return $this->search($request->input('query'), 'peraturan_pemerintah', 'id', 'perihal');
    }
    
    public function sumberPengetahuanBaru(Request $request)
    {        
        return $this->search($request->input('query'), 'sumber_pengetahuan_baru', 'id', 'subject');
    }
    
}
