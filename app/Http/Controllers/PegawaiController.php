<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePegawaiRequest;
use App\Http\Requests\UpdatePegawaiRequest;
use App\Repositories\PegawaiRepository;
use App\Repositories\TupoksiJabatanRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PegawaiController extends AppBaseController
{
    /** @var  PegawaiRepository */
    private $pegawaiRepository;

    /** @var  TupoksiJabatanRepository */
    private $tupoksiJabatanRepository;

    public function __construct(PegawaiRepository $pegawaiRepo, tupoksiJabatanRepository $tupoksiJabatanRepo)
    {
        $this->middleware('auth');
        $this->pegawaiRepository = $pegawaiRepo;
        $this->tupoksiJabatanRepository = $tupoksiJabatanRepo;
    }

    /**
     * Display a listing of the Pegawai.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pegawaiRepository->pushCriteria(new RequestCriteria($request));
        $pegawaiList = $this->pegawaiRepository->with(['user', 'tupoksi'])->all();

        return view('pegawai.index')
            ->with('pegawaiList', $pegawaiList);
    }

    /**
     * Show the form for creating a new Pegawai.
     *
     * @return Response
     */
    public function create()
    {
        $tupoksiList = $this->tupoksiJabatanRepository->pluck('nama_jabatan', 'id')->all();
        return view('pegawai.create')
            ->with('tupoksiList', $tupoksiList);
    }

    /**
     * Store a newly created Pegawai in storage.
     *
     * @param CreatePegawaiRequest $request
     *
     * @return Response
     */
    public function store(CreatePegawaiRequest $request)
    {        
        $pegawai = $this->pegawaiRepository->create($request->all());

        Flash::success('Pegawai saved successfully.');

        return redirect(route('pegawai.index'));
    }

    /**
     * Display the specified Pegawai.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pegawai = $this->pegawaiRepository->with(['user', 'tupoksi'])->findWithoutFail($id);

        if (empty($pegawai)) {
            Flash::error('Pegawai not found');

            return redirect(route('pegawai.index'));
        }

        return view('pegawai.show')->with('pegawai', $pegawai);
    }

    /**
     * Show the form for editing the specified Pegawai.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pegawai = $this->pegawaiRepository->with(['user', 'tupoksi'])->findWithoutFail($id);

        if (empty($pegawai)) {
            Flash::error('Pegawai not found');

            return redirect(route('pegawai.index'));
        }
        $tupoksiList = $this->tupoksiJabatanRepository->pluck('nama_jabatan', 'id')->all();
        return view('pegawai.edit')->with('pegawai', $pegawai)->with('tupoksiList', $tupoksiList);
    }

    /**
     * Update the specified Pegawai in storage.
     *
     * @param  int              $id
     * @param UpdatePegawaiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePegawaiRequest $request)
    {
        $pegawai = $this->pegawaiRepository->findWithoutFail($id);

        if (empty($pegawai)) {
            Flash::error('Pegawai not found');

            return redirect(route('pegawai.index'));
        }

        $pegawai = $this->pegawaiRepository->update($request->all(), $pegawai);

        Flash::success('Pegawai updated successfully.');

        return redirect(route('pegawai.index'));
    }

    /**
     * Remove the specified Pegawai from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pegawai = $this->pegawaiRepository->findWithoutFail($id);

        if (empty($pegawai)) {
            Flash::error('Pegawai not found');

            return redirect(route('pegawai.index'));
        }

        $this->pegawaiRepository->delete($id);

        Flash::success('Pegawai deleted successfully.');

        return redirect(route('pegawai.index'));
    }
}
