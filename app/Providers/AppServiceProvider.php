<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Form;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // fix issue with migration to MariaDB
        Schema::defaultStringLength(191);

        // force https
        $appUrl = env('APP_URL');
        if (starts_with(strtolower($appUrl), 'https')) {
            URL::forceScheme('https');
        }

        // custom components
        Form::component('datePicker', 'components.form.datePicker', ['name', 'value' => null, 'attributes' => []]);
        Form::component('datetimePicker', 'components.form.datetimePicker', ['name', 'value' => null, 'attributes' => []]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
