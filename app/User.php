<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $cascadeDeletes = ['pegawai'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'role', 'email', 'password', 'no_telp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const ROLES = [
        'ADMINISTRATOR' => 0,
        'PEGAWAI' => 1,
        'EXPERT' => 2,
    ];


    /**
     * Get pegawai
     */
    public function pegawai()
    {
        return $this->hasOne('App\Models\Pegawai');
    }    

    /**
     * Get pegawai
     */
    public function expert()
    {
        return $this->hasOne('App\Models\Expert');
    }    

    /**
     * Get the SumberPengetahuanBaru
     */
    public function sumberPengetahuanBaru()
    {
        return $this->hasMany('App\Models\SumberPengetahuanBaru');
    }

    /**
     * Get the Kasus
     */
    public function kasus()
    {
        return $this->hasMany('App\Models\Kasus');
    }
     
    
    public function chats()
    {
        return $this->belongsToMany('App\Models\ChatSession');
    }    
    
    /**
     * Encrypt password
     */
    public function setPasswordAttribute($password){
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Send a password reset email to the user
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordToken($token));
    }    
}
