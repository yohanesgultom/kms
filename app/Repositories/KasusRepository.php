<?php

namespace App\Repositories;

use App\Models\Kasus;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class KasusRepository
 * @package App\Repositories
 * @version November 17, 2017, 1:06 pm UTC
 *
 * @method Kasus findWithoutFail($id, $columns = ['*'])
 * @method Kasus find($id, $columns = ['*'])
 * @method Kasus first($columns = ['*'])
*/
class KasusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kasus',
        'kata_kunci',
        'indikator',
        'solusi',
        'alat',
        'rate',
        'tanggal'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Kasus::class;
    }
}
