<?php

namespace App\Repositories;

use App\Models\Pegawai;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityUpdated;

/**
 * Class PegawaiRepository
 * @package App\Repositories
 * @version November 14, 2017, 3:10 pm UTC
 *
 * @method Pegawai findWithoutFail($id, $columns = ['*'])
 * @method Pegawai find($id, $columns = ['*'])
 * @method Pegawai first($columns = ['*'])
*/
class PegawaiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'NIP_NIK',
        'no_telp',
        'seksi_subbagian',
        'tmt_jabatan',
        'tupoksi_jabatan_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pegawai::class;
    }

    /**
     * Save a new entity in repository
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes)
    {
        $userAttributes = $attributes['user'];

        if (!is_null($this->validator)) {
            // we should pass data that has been casts by the model
            // to make sure data type are same because validator may need to use
            // this data to compare with data that fetch from database.
            $attributes = $this->model->newInstance()->forceFill($attributes)->toArray();

            $this->validator->with($attributes)->passesOrFail(ValidatorInterface::RULE_CREATE);
        }

        // create User then Pegawai
        $model = $this->model->newInstance();
        DB::transaction(function () use (&$model, $attributes, $userAttributes) {
            $user = \App\User::create($userAttributes);
            $attributes['user_id'] = $user->id;
            $model = \App\Models\Pegawai::create($attributes);
        });

        $this->resetModel();

        event(new RepositoryEntityCreated($this, $model));

        return $this->parserResult($model);
    }

    
        /**
     * Update a entity in repository by model
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     * @param       $model
     *
     * @return mixed
     */
    public function update(array $attributes, $model)
    {
        $userAttributes = $attributes['user'];

        $this->applyScope();        

        if (!is_null($this->validator)) {
            // we should pass data that has been casts by the model
            // to make sure data type are same because validator may need to use
            // this data to compare with data that fetch from database.
            $attributes = $this->model->newInstance()->forceFill($attributes)->toArray();

            $this->validator->with($attributes)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        }

        $temporarySkipPresenter = $this->skipPresenter;

        $this->skipPresenter(true);

        // update User then Pegawai
        DB::transaction(function () use (&$model, $attributes, $userAttributes) {
            $user = $model->user;
            $user->fill($userAttributes);
            $user->save();
            $model->fill($attributes);
            $model->save();
        });

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        event(new RepositoryEntityUpdated($this, $model));

        return $this->parserResult($model);
    }

}
