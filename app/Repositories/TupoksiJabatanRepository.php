<?php

namespace App\Repositories;

use App\Models\TupoksiJabatan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TupoksiJabatanRepository
 * @package App\Repositories
 * @version November 12, 2017, 11:37 am UTC
 *
 * @method TupoksiJabatan findWithoutFail($id, $columns = ['*'])
 * @method TupoksiJabatan find($id, $columns = ['*'])
 * @method TupoksiJabatan first($columns = ['*'])
*/
class TupoksiJabatanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_jabatan'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TupoksiJabatan::class;
    }
}
