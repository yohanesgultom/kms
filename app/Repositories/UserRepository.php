<?php

namespace App\Repositories;

use App\User;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserRepository
 * @package namespace App\Repositories;
 * @version December 11, 2017, 11:37 am UTC
 * 
 * @method User findWithoutFail($id, $columns = ['*'])
 * @method User find($id, $columns = ['*'])
 * @method User first($columns = ['*'])
 */
class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'role',
        'username',
        'no_telp'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
    
}
