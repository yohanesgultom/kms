<?php

namespace App\Repositories;

use App\Models\KnowledgeGap;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class KnowledgeGapRepository
 * @package App\Repositories
 * @version November 17, 2017, 1:02 pm UTC
 *
 * @method KnowledgeGap findWithoutFail($id, $columns = ['*'])
 * @method KnowledgeGap find($id, $columns = ['*'])
 * @method KnowledgeGap first($columns = ['*'])
*/
class KnowledgeGapRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kepentingan',
        'pemahaman'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return KnowledgeGap::class;
    }
}
