<?php

namespace App\Repositories;

use App\User;
use App\Models\ChatSession;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class ChatSessionRepository
 * @package App\Repositories
 * @version December 23, 2017, 2:16 pm UTC
 *
 * @method ChatSession findWithoutFail($id, $columns = ['*'])
 * @method ChatSession find($id, $columns = ['*'])
 * @method ChatSession first($columns = ['*'])
*/
class ChatSessionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ChatSession::class;
    }

    public function findOrCreate($userIds)
    {
        $bindingStr = implode(',', array_fill(0, count($userIds), '?'));
        $params = array_merge([], $userIds);
        array_push($params, count($userIds));

        $sql = "SELECT s.id, COUNT(su.id) FROM chat_sessions s JOIN chat_session_user su ON s.id = su.chat_session_id WHERE su.user_id IN ($bindingStr) GROUP BY s.id HAVING COUNT(su.id) = ?";

        $results = DB::select($sql, $params);
        if (empty($results)) {
            // create new session and add users
            $session = ChatSession::create([]);
            $session->users()->attach($userIds);
        } else {
            $session = ChatSession::with(['users', 'messages'])->find($results[0]->id);
        }
        return $session;
    }
}
