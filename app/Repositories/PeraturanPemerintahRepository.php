<?php

namespace App\Repositories;

use App\Models\PeraturanPemerintah;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PeraturanPemerintahRepository
 * @package App\Repositories
 * @version November 17, 2017, 12:31 pm UTC
 *
 * @method PeraturanPemerintah findWithoutFail($id, $columns = ['*'])
 * @method PeraturanPemerintah find($id, $columns = ['*'])
 * @method PeraturanPemerintah first($columns = ['*'])
*/
class PeraturanPemerintahRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jenis',
        'tanggal'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PeraturanPemerintah::class;
    }
}
