<?php

namespace App\Repositories;

use App\Models\SumberPengetahuanBaru;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SumberPengetahuanBaruRepository
 * @package App\Repositories
 * @version November 17, 2017, 1:11 pm UTC
 *
 * @method SumberPengetahuanBaru findWithoutFail($id, $columns = ['*'])
 * @method SumberPengetahuanBaru find($id, $columns = ['*'])
 * @method SumberPengetahuanBaru first($columns = ['*'])
*/
class SumberPengetahuanBaruRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jenis',
        'subject',
        'kata_kunci',
        'tanggal'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SumberPengetahuanBaru::class;
    }
}
