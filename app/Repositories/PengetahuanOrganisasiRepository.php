<?php

namespace App\Repositories;

use App\Models\PengetahuanOrganisasi;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PengetahuanOrganisasiRepository
 * @package App\Repositories
 * @version November 17, 2017, 12:53 pm UTC
 *
 * @method PengetahuanOrganisasi findWithoutFail($id, $columns = ['*'])
 * @method PengetahuanOrganisasi find($id, $columns = ['*'])
 * @method PengetahuanOrganisasi first($columns = ['*'])
*/
class PengetahuanOrganisasiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_pengetahuan',
        'tanggal'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PengetahuanOrganisasi::class;
    }
}
