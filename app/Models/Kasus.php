<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Kasus
 * @package App\Models
 * @version November 17, 2017, 1:06 pm UTC
 *
 * @property string kasus
 * @property string kata_kunci
 * @property string indikator
 * @property string solusi
 * @property string alat
 * @property binary upload_foto
 * @property binary upload_dokumen
 * @property binary upload_video
 * @property binary upload_audio
 * @property string feedback
 * @property integer rate
 * @property date tanggal
 */
class Kasus extends Model
{
    use SoftDeletes;

    public $table = 'kasus';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'kasus',
        'kata_kunci',
        'indikator',
        'solusi',
        'alat',
        'upload_foto',
        'upload_dokumen',
        'upload_video',
        'upload_audio',
        'feedback',
        'rate',
        'created_by',
        'updated_by',
        'user_id',
        'pengetahuan_organisasi_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kasus' => 'string',
        'kata_kunci' => 'string',
        'indikator' => 'string',
        'solusi' => 'string',
        'alat' => 'string',
        'feedback' => 'string',
        'rate' => 'integer',
        'tanggal' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'kasus' => 'required'
    ];

    
    /**
     * Get User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    /**
     * Get Pengetahuan Organisasi
     */
    public function pengetahuanOrganisasi()
    {
        return $this->belongsTo('App\Models\PengetahuanOrganisasi');
    }    
    
}
