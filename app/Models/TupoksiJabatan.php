<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TupoksiJabatan
 * @package App\Models
 * @version November 12, 2017, 11:37 am UTC
 *
 * @property string nama_jabatan
 * @property string ikhtisar_jabatan
 * @property string uraian_tugas
 * @property string hasil_kerja
 * @property string tanggung_jawab
 * @property string syarat_jabatan
 * @property binary upload_dokumen
 */
class TupoksiJabatan extends Model
{
    use SoftDeletes;

    public $table = 'tupoksi_jabatan';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_jabatan',
        'ikhtisar_jabatan',
        'uraian_tugas',
        'hasil_kerja',
        'tanggung_jawab',
        'syarat_jabatan',
        'upload_dokumen',
        'created_by',
        'updated_by'
];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama_jabatan' => 'string',
        'ikhtisar_jabatan' => 'string',
        'uraian_tugas' => 'string',
        'hasil_kerja' => 'string',
        'tanggung_jawab' => 'string',
        'syarat_jabatan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_jabatan' => 'required'
    ];

    
    /**
     * get pegawai
     */
    public function pegawai()
    {
        return $this->hasMany('App\Models\Pegawai');
    }    
}
