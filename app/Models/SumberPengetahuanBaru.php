<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SumberPengetahuanBaru
 * @package App\Models
 * @version November 17, 2017, 1:11 pm UTC
 *
 * @property integer jenis
 * @property string subject
 * @property string kata_kunci
 * @property string deskripsi
 * @property binary upload_dokumen
 * @property binary upload_foto
 * @property binary upload_video
 * @property binary upload_audio
 */
class SumberPengetahuanBaru extends Model
{
    use SoftDeletes;

    public $table = 'sumber_pengetahuan_baru';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'jenis',
        'subject',
        'kata_kunci',
        'deskripsi',
        'upload_dokumen',
        'upload_foto',
        'upload_video',
        'upload_audio',
        'user_id',
        'pengetahuan_organisasi_id',
        'created_by',
        'updated_by'
   ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'jenis' => 'integer',
        'subject' => 'string',
        'kata_kunci' => 'string',
        'deskripsi' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'subject' => 'required'
    ];

    /**
     * Jenis
     * 
     * @var array
     */
    const JENIS = [
        0 => 'Lain-Lain',
        1 => 'SOP',
        2 => 'Acuan Kerja',
        3 => 'Laporan',
        4 => 'Bahan Ajar',
    ];
    
    /**
     * Get User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    

    /**
     * Get Pengetahuan Organisasi
     */
    public function pengetahuanOrganisasi()
    {
        return $this->belongsTo('App\Models\PengetahuanOrganisasi');
    }
    
}
