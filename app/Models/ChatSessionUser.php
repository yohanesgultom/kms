<?php

namespace App\Models;

use Eloquent as Model;

class ChatSessionUser extends Model
{
    public $table = 'chat_session_user';

    public $fillable = [
        'message_count',
        'chat_session_id',
        'user_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'message_count' => 'integer',
        'chat_session_id' => 'integer',
        'user_id' => 'integer',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }    

    public function chat()
    {
        return $this->belongsTo('App\Models\ChatSession', 'chat_session_id');
    }    
    
}
