<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ChatMessage
 * @package App\Models
 * @version December 23, 2017, 2:24 pm UTC
 *
 * @property integer user_id
 * @property string message
 */
class ChatMessage extends Model
{
    public $table = 'chat_messages';

    public $fillable = [
        'user_id',
        'message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'message' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required'
    ];

    public function chat()
    {
        return $this->belongsTo('App\Models\ChatSession');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }    

}
