<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class KnowledgeGap
 * @package App\Models
 * @version November 17, 2017, 1:02 pm UTC
 *
 * @property integer kepentingan
 * @property integer pemahaman
 */
class KnowledgeGap extends Model
{
    use SoftDeletes;

    public $table = 'knowledge_gaps';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'kepentingan',
        'pemahaman',
        'pegawai_id',
        'pengetahuan_organisasi_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kepentingan' => 'integer',
        'pemahaman' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'kepentingan' => 'required',
        'pemahaman' => 'required'
    ];

    /**
     * Get Pegawai
     */
    public function pegawai()
    {
        return $this->belongsTo('App\Models\Pegawai');
    }
    
    /**
     * Get Pengetahuan Organisasi
     */
    public function pengetahuanOrganisasi()
    {
        return $this->belongsTo('App\Models\PengetahuanOrganisasi');
    }    
    
    /**
     * Get calculated gap
     */
    public function getGapAttribute() {
        return $this->kepentingan - $this->pemahaman;
    }  
      
}
