<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

/**
 * Class Expert
 * @package App\Models
 * @version November 17, 2017, 9:02 am UTC
 *
 * @property dateTime available_date
 * @property string keahlian
 */
class Expert extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    public $table = 'experts';
    
    protected $dates = ['deleted_at'];

    protected $cascadeDeletes = ['user'];

    public $fillable = [
        'available_date',
        'keahlian',
        'user_id',
        'pengetahuan_organisasi_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'available_date' => 'datetime',
        'keahlian' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Get user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get Pengetahuan Organisasi
     */
    public function pengetahuanOrganisasi()
    {
        return $this->belongsTo('App\Models\PengetahuanOrganisasi');
    }    

}
