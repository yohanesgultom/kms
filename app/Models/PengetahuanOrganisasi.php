<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PengetahuanOrganisasi
 * @package App\Models
 * @version November 17, 2017, 12:53 pm UTC
 *
 * @property string nama_pengetahuan
 * @property string deskripsi_pengetahuan
 * @property binary upload_dokumen
 * @property date tanggal
 */
class PengetahuanOrganisasi extends Model
{
    use SoftDeletes;

    public $table = 'pengetahuan_organisasi';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_pengetahuan',
        'deskripsi_pengetahuan',
        'upload_dokumen',
        'tanggal',
        'created_by',
        'updated_by'        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama_pengetahuan' => 'string',
        'deskripsi_pengetahuan' => 'string',
        'tanggal' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_pengetahuan' => 'required',
    ];


    /**
     * Get Experts
     */
    public function experts()
    {
        return $this->hasMany('App\Models\Expert');
    }    

    /**
     * Get Peraturan Pemerintah
     */
    public function peraturanPemerintah()
    {
        return $this->hasMany('App\Models\PeraturanPemerintah');
    }    

    /**
     * Get Knowlege Gaps
     */
    public function knowledgeGaps()
    {
        return $this->hasMany('App\Models\KnowledgeGap');
    }    

    /**
     * Get Sumber Pengetahuan Baru
     */
    public function sumberPengetahuanBaru()
    {
        return $this->hasMany('App\Models\SumberPengetahuanBaru');
    }    

    /**
     * Get Knowlege Kasus
     */
    public function kasus()
    {
        return $this->hasMany('App\Models\Kasus');
    }    
    
}
