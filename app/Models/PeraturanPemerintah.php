<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PeraturanPemerintah
 * @package App\Models
 * @version November 17, 2017, 12:31 pm UTC
 *
 * @property tinyInteger jenis
 * @property string perihal
 * @property binary upload_dokumen
 * @property date tanggal
 */
class PeraturanPemerintah extends Model
{
    use SoftDeletes;

    public $table = 'peraturan_pemerintah';
    
    protected $dates = ['deleted_at'];


    public $fillable = [
        'jenis',
        'perihal',
        'upload_dokumen',
        'tanggal',
        'created_by',
        'updated_by',
        'pengetahuan_organisasi_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'perihal' => 'string',
        'tanggal' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Jenis Peraturan Pemerintah
     * 
     * @var array
     */
    const JENIS = [
        0 => 'LAIN-LAIN',
        1 => 'KEPMEN',
        2 => 'PERMEN',
        3 => 'KEPRES',
    ];

    /**
     * Get Pengetahuan Organisasi
     */
    public function pengetahuanOrganisasi()
    {
        return $this->belongsTo('App\Models\PengetahuanOrganisasi');
    }    

    /**
     * Set Jenis peraturan
     *
     * @param  string  $value
     * @return void
     */
    public function setJenisAttribute($value)
    {
        if (gettype($value) != 'integer') {
            try {
                $value = intval("$value");
            } catch (\Exception $e) {
                $value = 0;
            }
        }
        $this->attributes['jenis'] = $value;
    }    
}
