<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ChatSession
 * @package App\Models
 * @version December 23, 2017, 2:16 pm UTC
 *
 */
class ChatSession extends Model
{
    public $table = 'chat_sessions';
    
    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }    

    public function messages()
    {
        return $this->hasMany('App\Models\ChatMessage')->orderBy('id');
    }    
    
}
