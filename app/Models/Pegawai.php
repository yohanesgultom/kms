<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

/**
 * Class Pegawai
 * @package App\Models
 * @version November 14, 2017, 3:10 pm UTC
 *
 * @property string NIP_NIK
 * @property string nama
 * @property string no_telp
 * @property string seksi_subbagian
 * @property string tmt_jabatan
 * @property integer tupoksi_jabatan_id
 */
class Pegawai extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    public $table = 'pegawai';
    
    protected $dates = ['deleted_at'];

    protected $cascadeDeletes = ['user'];

    public $fillable = [
        'NIP_NIK',
        'no_telp',
        'seksi_subbagian',
        'tmt_jabatan',
        'tupoksi_jabatan_id',
        'user_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'NIP_NIK' => 'string',
        'no_telp' => 'string',
        'seksi_subbagian' => 'string',
        'tmt_jabatan' => 'string',
        'tupoksi_jabatan_id' => 'integer',
        'user_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'NIP_NIK' => 'required',
    ];

    /**
     * Get user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get Knowledge Gaps
     */
    public function knowledgeGaps()
    {
        return $this->hasMany('App\Models\KnowledgeGap');
    }
    
    /**
     * Get tupoksi/jabatan
     */
    public function tupoksi()
    {
        return $this->belongsTo('App\Models\TupoksiJabatan', 'tupoksi_jabatan_id');
    }
}
