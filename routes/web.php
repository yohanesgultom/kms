<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::get('rekomendasiPengetahuan', 'RekomendasiPengetahuanController@index')->name('rekomendasiPengetahuan.index');
Route::prefix('rekomendasiPengetahuan')->group(function () {
    Route::get('peraturanPemerintah', 'RekomendasiPengetahuanController@peraturanPemerintah')->name('rekomendasiPengetahuan.peraturanPemerintah');
    Route::get('sumberPengetahuanBaru', 'RekomendasiPengetahuanController@sumberPengetahuanBaru')->name('rekomendasiPengetahuan.sumberPengetahuanBaru');
    Route::get('kasus', 'RekomendasiPengetahuanController@kasus')->name('rekomendasiPengetahuan.kasus');
    Route::get('experts', 'RekomendasiPengetahuanController@experts')->name('rekomendasiPengetahuan.experts');
});

Route::prefix('chat')->group(function () {
    Route::get('count', 'ChatSessionController@count');
    Route::post('start', 'ChatSessionController@start');
    Route::get('load/{id}', 'ChatSessionController@load');
    Route::post('send/{id}', 'ChatSessionController@send');
    Route::get('sync/{id}', 'ChatSessionController@sync');
});

Route::get('search', 'SearchController@index')->name('search.index');
Route::prefix('search')->group(function () {
    Route::get('kasus', 'SearchController@kasus');
    Route::get('pengetahuanOrganisasi', 'SearchController@pengetahuanOrganisasi');
    Route::get('peraturanPemerintah', 'SearchController@peraturanPemerintah');
    Route::get('sumberPengetahuanBaru', 'SearchController@sumberPengetahuanBaru');
});


Route::resource('tupoksiJabatan', 'TupoksiJabatanController');
Route::resource('pegawai', 'PegawaiController');
Route::resource('experts', 'ExpertController');
Route::resource('peraturanPemerintah', 'PeraturanPemerintahController');
Route::resource('pengetahuanOrganisasi', 'PengetahuanOrganisasiController');
Route::resource('knowledgeGaps', 'KnowledgeGapController');
Route::resource('kasus', 'KasusController');
Route::resource('sumberPengetahuanBaru', 'SumberPengetahuanBaruController');