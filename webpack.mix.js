let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
    'resources/assets/js/chat.js',
    'resources/assets/js/app.js'
    ], 'public/js/app.js')
   .styles([
       'resources/assets/css/AdminLTE.css',
       'resources/assets/css/chat.css'
    ], 'public/css/app.min.css');