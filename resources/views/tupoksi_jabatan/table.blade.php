<table class="table table-responsive" id="tupoksiJabatans-table">
    <thead>
        <tr>
            <th>Nama Jabatan</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tupoksiJabatans as $tupoksiJabatan)
        <tr>
            <td>{!! $tupoksiJabatan->nama_jabatan !!}</td>
            <td>
                {!! Form::open(['route' => ['tupoksiJabatan.destroy', $tupoksiJabatan->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tupoksiJabatan.show', [$tupoksiJabatan->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tupoksiJabatan.edit', [$tupoksiJabatan->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>