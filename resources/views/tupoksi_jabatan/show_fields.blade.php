<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tupoksiJabatan->id !!}</p>
</div>

<!-- Nama Jabatan Field -->
<div class="form-group">
    {!! Form::label('nama_jabatan', 'Nama Jabatan:') !!}
    <p>{!! $tupoksiJabatan->nama_jabatan !!}</p>
</div>

<!-- Ikhtisar Jabatan Field -->
<div class="form-group">
    {!! Form::label('ikhtisar_jabatan', 'Ikhtisar Jabatan:') !!}
    <p>{!! $tupoksiJabatan->ikhtisar_jabatan !!}</p>
</div>

<!-- Uraian Tugas Field -->
<div class="form-group">
    {!! Form::label('uraian_tugas', 'Uraian Tugas:') !!}
    <p>{!! $tupoksiJabatan->uraian_tugas !!}</p>
</div>

<!-- Hasil Kerja Field -->
<div class="form-group">
    {!! Form::label('hasil_kerja', 'Hasil Kerja:') !!}
    <p>{!! $tupoksiJabatan->hasil_kerja !!}</p>
</div>

<!-- Tanggung Jawab Field -->
<div class="form-group">
    {!! Form::label('tanggung_jawab', 'Tanggung Jawab:') !!}
    <p>{!! $tupoksiJabatan->tanggung_jawab !!}</p>
</div>

<!-- Syarat Jabatan Field -->
<div class="form-group">
    {!! Form::label('syarat_jabatan', 'Syarat Jabatan:') !!}
    <p>{!! $tupoksiJabatan->syarat_jabatan !!}</p>
</div>

<!-- Upload Dokumen Field -->
<div class="form-group">
    {!! Form::label('upload_dokumen', 'Upload Dokumen:') !!}
    @if (!empty($tupoksiJabatan) && !empty($tupoksiJabatan->upload_dokumen))
    <p><embed src="data:application/pdf;base64,{!! base64_encode($tupoksiJabatan->upload_dokumen) !!}" width="500" height="375" type='application/pdf'></p>
    @endif
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tupoksiJabatan->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tupoksiJabatan->updated_at !!}</p>
</div>

