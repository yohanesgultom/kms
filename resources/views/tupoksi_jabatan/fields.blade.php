<!-- Nama Jabatan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_jabatan', 'Nama Jabatan:') !!}
    {!! Form::text('nama_jabatan', null, ['class' => 'form-control']) !!}
</div>

<!-- Ikhtisar Jabatan Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('ikhtisar_jabatan', 'Ikhtisar Jabatan:') !!}
    {!! Form::textarea('ikhtisar_jabatan', null, ['class' => 'form-control']) !!}
</div>

<!-- Uraian Tugas Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('uraian_tugas', 'Uraian Tugas:') !!}
    {!! Form::textarea('uraian_tugas', null, ['class' => 'form-control']) !!}
</div>

<!-- Hasil Kerja Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('hasil_kerja', 'Hasil Kerja:') !!}
    {!! Form::textarea('hasil_kerja', null, ['class' => 'form-control']) !!}
</div>

<!-- Tanggung Jawab Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('tanggung_jawab', 'Tanggung Jawab:') !!}
    {!! Form::textarea('tanggung_jawab', null, ['class' => 'form-control']) !!}
</div>

<!-- Syarat Jabatan Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('syarat_jabatan', 'Syarat Jabatan:') !!}
    {!! Form::textarea('syarat_jabatan', null, ['class' => 'form-control']) !!}
</div>

<!-- Upload Dokumen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('upload_dokumen', 'Upload Dokumen:') !!}
    {!! Form::file('upload_dokumen') !!}
    @if (!empty($tupoksiJabatan) && !empty($tupoksiJabatan->upload_dokumen))
    <p><embed src="data:application/pdf;base64,{!! base64_encode($tupoksiJabatan->upload_dokumen) !!}" width="500" height="375" type='application/pdf'></p>
    @endif
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}    
    <a href="{!! route('tupoksiJabatan.index') !!}" class="btn btn-default">Cancel</a>
</div>
