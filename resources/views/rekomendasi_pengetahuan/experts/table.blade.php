<table class="table table-responsive" id="experts-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Available Date</th>
            <th>Keahlian</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($experts as $expert)
        <tr>
            <td>{!! $expert->user->name !!}</td>
            <td>{!! $expert->user->email !!}</td>
            <td>{!! $expert->available_date !!}</td>
            <td>{!! str_limit($expert->keahlian, 20) !!}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('experts.show', [$expert->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>