@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Rekomendasi Pengetahuan</h1>        
    </section>
    <div class="content">
        
        <div class="clearfix"></div>
        <h4>Experts</h4>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('rekomendasi_pengetahuan.experts.table')
            </div>
        </div>
    </div>
@endsection

