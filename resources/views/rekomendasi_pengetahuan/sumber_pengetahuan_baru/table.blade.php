<table class="table table-responsive" id="sumberPengetahuanBaru-table">
    <thead>
        <tr>
            <th>Title</th>
            <th>Created By</th>
            <th>Created At</th>
            <th>Jenis</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($sumberPengetahuanBaru as $sumberPengetahuanBaru)
        <tr>
            <td>{!! $sumberPengetahuanBaru->subject !!}</td>
            <td>{!! $sumberPengetahuanBaru->created_by !!}</td>
            <td>{!! $sumberPengetahuanBaru->created_at !!}</td>
            <td>{!! \App\Models\SumberPengetahuanBaru::JENIS[$sumberPengetahuanBaru->jenis] !!}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('sumberPengetahuanBaru.show', [$sumberPengetahuanBaru->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>