@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Peraturan Pemerintah
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'peraturanPemerintah.store', 'enctype' => 'multipart/form-data']) !!}

                        @include('peraturan_pemerintah.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
