<!-- Jenis Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis', 'Jenis:') !!}
    {!! Form::select('jenis', \App\Models\PeraturanPemerintah::JENIS, null, ['class' => 'form-control']) !!}
</div>

<!-- Pengetahuan Organisasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pengetahuan_organisasi_id', 'Pengetahuan Organisasi:') !!}
    {!! Form::select('pengetahuan_organisasi_id', $pengetahuanList, null, ['class' => 'form-control', 'placeholder' => 'Please Select..']) !!}
</div>

<!-- Perihal Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('perihal', 'Perihal:') !!}
    {!! Form::textarea('perihal', null, ['class' => 'form-control']) !!}
</div>

<!-- Upload Dokumen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('upload_dokumen', 'Upload Dokumen:') !!}
    {!! Form::file('upload_dokumen') !!}
    @if (!empty($peraturanPemerintah) && !empty($peraturanPemerintah->upload_dokumen))
    <p><embed src="data:application/pdf;base64,{!! base64_encode($peraturanPemerintah->upload_dokumen) !!}" width="500" height="375" type='application/pdf'></p>
    @endif
</div>
<div class="clearfix"></div>

<!-- Tanggal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::datePicker('tanggal', empty($peraturanPemerintah) ? null : $peraturanPemerintah->tanggal->format($dateFormat), ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('peraturanPemerintah.index') !!}" class="btn btn-default">Cancel</a>
</div>
