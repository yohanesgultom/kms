<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $peraturanPemerintah->id !!}</p>
</div>

<!-- Jenis Field -->
<div class="form-group">
    {!! Form::label('jenis', 'Jenis:') !!}
    <p>{!! \App\Models\PeraturanPemerintah::JENIS[$peraturanPemerintah->jenis] !!}</p>
</div>

<!-- Pengetahuan Organisasi Field -->
<div class="form-group">
    {!! Form::label('pengetahuan_organisasi_id', 'Pengetahuan Organisasi:') !!}
    <p>{!! $peraturanPemerintah->pengetahuanOrganisasi->nama_pengetahuan !!}</p>
</div>

<!-- Perihal Field -->
<div class="form-group">
    {!! Form::label('perihal', 'Perihal:') !!}
    <p>{!! $peraturanPemerintah->perihal !!}</p>
</div>

<!-- Upload Dokumen Field -->
<div class="form-group">
    {!! Form::label('upload_dokumen', 'Upload Dokumen:') !!}
    @if (!empty($peraturanPemerintah) && !empty($peraturanPemerintah->upload_dokumen))
    <p><embed src="data:application/pdf;base64,{!! base64_encode($peraturanPemerintah->upload_dokumen) !!}" width="500" height="375" type='application/pdf'></p>
    @endif
</div>

<!-- Tanggal Field -->
<div class="form-group">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{!! $peraturanPemerintah->tanggal !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created By:') !!}
    <p>{!! $peraturanPemerintah->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $peraturanPemerintah->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $peraturanPemerintah->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $peraturanPemerintah->updated_at !!}</p>
</div>
