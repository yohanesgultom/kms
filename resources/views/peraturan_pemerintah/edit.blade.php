@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Peraturan Pemerintah
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($peraturanPemerintah, ['route' => ['peraturanPemerintah.update', $peraturanPemerintah->id, 'enctype' => 'multipart/form-data'], 'method' => 'patch']) !!}

                        @include('peraturan_pemerintah.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection