<table class="table table-responsive" id="peraturanPemerintah-table">
    <thead>
        <tr>
            <th>Perihal</th>
            <th>Jenis</th>
            <th>Tanggal</th>
            <th>Created By</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($peraturanPemerintah as $peraturanPemerintah)
        <tr>
            <td>{!! str_limit($peraturanPemerintah->perihal, 50) !!}</td>
            <td>{!! \App\Models\PeraturanPemerintah::JENIS[$peraturanPemerintah->jenis] !!}</td>
            <td>{!! $peraturanPemerintah->tanggal->format($dateFormat) !!}</td>
            <td>{!! $peraturanPemerintah->created_by !!}</td>
            <td>
                {!! Form::open(['route' => ['peraturanPemerintah.destroy', $peraturanPemerintah->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('peraturanPemerintah.show', [$peraturanPemerintah->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('peraturanPemerintah.edit', [$peraturanPemerintah->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>