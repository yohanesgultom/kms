@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Search Result for "<span id="query">{{ $query }}</span>"</h1>
</section>
<div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <br>

        <!-- Kasus -->
        <div class="row">
            <div class="col-md-12">
                <div id="result-kasus" class="box box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Kasus</h3>
                    </div>
                    <div class="box-body"></div>
                    <div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                </div>
            </div>        
        </div>

        <!-- Pengetahuan Organisasi -->
        <div class="row">
            <div class="col-md-12">
                <div id="result-pengetahuan-organisasi" class="box box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Pengetahuan Organisasi</h3>
                    </div>
                    <div class="box-body"></div>
                    <div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                </div>
            </div>        
        </div>

        <!-- Peraturan Pemerintah -->
        <div class="row">
            <div class="col-md-12">
                <div id="result-peraturan-pemerintah" class="box box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Peraturan Pemerintah</h3>
                    </div>
                    <div class="box-body"></div>
                    <div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                </div>
            </div>        
        </div>

        <!-- Sumber Pengetahuan Baru -->
        <div class="row">
            <div class="col-md-12">
                <div id="result-sumber-pengetahuan-baru" class="box box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Sumber Pengetahuan Baru</h3>
                    </div>
                    <div class="box-body"></div>
                    <div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                </div>
            </div>        
        </div>

    </div>
@endsection

@section('scripts')
<script>

const search = function (url, container, controller, idField, valField) {
$.ajax({
    url: url,
    type: 'get',
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    dataType: 'json'
})
    .done(function (res) {
        let box = container.find('.box-body');
        box.empty();
        if (res.data.length <= 0) {
            box.append($(`<div>No result</div>`));
        } else {
            for (let i = 0; i < res.data.length; i++) {
                let row = res.data[i];
                box.append($(`<div><a href="/${controller}/${row[idField]}">${row[valField]}</a></div>`));
            }
        }
        container.find('.overlay').remove();
    })
    .fail(console.error);
}

// load search results
let query = $('#query').text();
search(`/search/kasus?query=${query}`, $('#result-kasus'), 'kasus', 'id', 'kasus');
search(`/search/pengetahuanOrganisasi?query=${query}`, $('#result-pengetahuan-organisasi'), 'pengetahuanOrganisasi', 'id', 'nama_pengetahuan');
search(`/search/peraturanPemerintah?query=${query}`, $('#result-peraturan-pemerintah'), 'peraturanPemerintah', 'id', 'perihal');
search(`/search/sumberPengetahuanBaru?query=${query}`, $('#result-sumber-pengetahuan-baru'), 'sumberPengetahuanBaru', 'id', 'subject');

</script>
@endsection