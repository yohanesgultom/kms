<table class="table table-responsive" id="pengetahuanOrganisasi-table">
    <thead>
        <tr>
            <th>Nama Pengetahuan</th>
            <th>Tanggal</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($pengetahuanOrganisasi as $pengetahuanOrganisasi)
        <tr>
            <td>{!! $pengetahuanOrganisasi->nama_pengetahuan !!}</td>
            <td>{!! $pengetahuanOrganisasi->tanggal->format($dateFormat) !!}</td>
            <td>
                {!! Form::open(['route' => ['pengetahuanOrganisasi.destroy', $pengetahuanOrganisasi->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('pengetahuanOrganisasi.show', [$pengetahuanOrganisasi->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('pengetahuanOrganisasi.edit', [$pengetahuanOrganisasi->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>