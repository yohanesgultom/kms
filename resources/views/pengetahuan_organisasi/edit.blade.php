@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pengetahuan Organisasi
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pengetahuanOrganisasi, ['route' => ['pengetahuanOrganisasi.update', $pengetahuanOrganisasi->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                        @include('pengetahuan_organisasi.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection