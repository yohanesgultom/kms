<!-- Nama Pengetahuan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_pengetahuan', 'Nama Pengetahuan:') !!}
    {!! Form::text('nama_pengetahuan', null, ['class' => 'form-control']) !!}
</div>

<!-- Deskripsi Pengetahuan Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('deskripsi_pengetahuan', 'Deskripsi Pengetahuan:') !!}
    {!! Form::textarea('deskripsi_pengetahuan', null, ['class' => 'form-control']) !!}
</div>

<!-- Upload Dokumen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('upload_dokumen', 'Upload Dokumen:') !!}
    {!! Form::file('upload_dokumen') !!}
    @if (!empty($pengetahuanOrganisasi) && !empty($pengetahuanOrganisasi->upload_dokumen))
    <p><embed src="data:application/pdf;base64,{!! base64_encode($pengetahuanOrganisasi->upload_dokumen) !!}" width="500" height="375" type='application/pdf'></p>
    @endif
</div>
<div class="clearfix"></div>

<!-- Tanggal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::datePicker('tanggal', empty($pengetahuanOrganisasi) ? null : $pengetahuanOrganisasi->tanggal->format($dateFormat), ['class' => 'form-control']) !!}    
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pengetahuanOrganisasi.index') !!}" class="btn btn-default">Cancel</a>
</div>
