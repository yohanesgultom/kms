<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $pengetahuanOrganisasi->id !!}</p>
</div>

<!-- Nama Pengetahuan Field -->
<div class="form-group">
    {!! Form::label('nama_pengetahuan', 'Nama Pengetahuan:') !!}
    <p>{!! $pengetahuanOrganisasi->nama_pengetahuan !!}</p>
</div>

<!-- Deskripsi Pengetahuan Field -->
<div class="form-group">
    {!! Form::label('deskripsi_pengetahuan', 'Deskripsi Pengetahuan:') !!}
    <p>{!! $pengetahuanOrganisasi->deskripsi_pengetahuan !!}</p>
</div>

<!-- Upload Dokumen Field -->
<div class="form-group">
    {!! Form::label('upload_dokumen', 'Upload Dokumen:') !!}
    @if (!empty($pengetahuanOrganisasi) && !empty($pengetahuanOrganisasi->upload_dokumen))
    <p><embed src="data:application/pdf;base64,{!! base64_encode($pengetahuanOrganisasi->upload_dokumen) !!}" width="500" height="375" type='application/pdf'></p>
    @endif
</div>


<!-- Tanggal Field -->
<div class="form-group">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{!! $pengetahuanOrganisasi->tanggal !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $pengetahuanOrganisasi->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $pengetahuanOrganisasi->updated_at !!}</p>
</div>

