<!-- Name -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('user[name]', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Email -->
<div class="form-group col-sm-6">
    {!! Form::label('Email', 'Email:') !!}
    {!! Form::email('user[email]', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Name -->
<div class="form-group col-sm-6">
    {!! Form::label('username', 'Username:') !!}
    {!! Form::text('user[username]', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Password -->
<div class="form-group col-sm-6">
    {!! Form::label('Password', 'Password:') !!}
    {!! Form::password('user[password]', ['class' => 'form-control']) !!}
</div>

<!-- No Telp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user[no_telp]', 'No Telp:') !!}
    {!! Form::text('user[no_telp]', null, ['class' => 'form-control']) !!}
</div>

<!-- Nip Nik Field -->
<div class="form-group col-sm-6">
    {!! Form::label('NIP_NIK', 'NIP/NIK:') !!}
    {!! Form::text('NIP_NIK', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Seksi Subbagian Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seksi_subbagian', 'Seksi Subbagian:') !!}
    {!! Form::text('seksi_subbagian', null, ['class' => 'form-control']) !!}
</div>

<!-- Tmt Jabatan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tmt_jabatan', 'Tmt Jabatan:') !!}
    {!! Form::text('tmt_jabatan', null, ['class' => 'form-control']) !!}
</div>

<!-- Tupoksi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tupoksi_jabatan_id', 'Tupoksi:') !!}
    {!! Form::select('tupoksi_jabatan_id', $tupoksiList, null, ['class' => 'form-control', 'placeholder' => 'Please Select..']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pegawai.index') !!}" class="btn btn-default">Cancel</a>
</div>
