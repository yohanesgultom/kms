<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $pegawai->id !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Name:') !!}
    <p>{!! $pegawai->user->name !!}</p>
</div>

<!-- Username Field -->
<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    <p>{!! $pegawai->user->username !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $pegawai->user->email !!}</p>
</div>

<!-- No Telp Field -->
<div class="form-group">
    {!! Form::label('no_telp', 'No Telp:') !!}
    <p>{!! $pegawai->user->no_telp !!}</p>
</div>

<!-- Nip Nik Field -->
<div class="form-group">
    {!! Form::label('NIP_NIK', 'Nip Nik:') !!}
    <p>{!! $pegawai->NIP_NIK !!}</p>
</div>

<!-- Seksi Subbagian Field -->
<div class="form-group">
    {!! Form::label('seksi_subbagian', 'Seksi Subbagian:') !!}
    <p>{!! $pegawai->seksi_subbagian !!}</p>
</div>

<!-- Tmt Jabatan Field -->
<div class="form-group">
    {!! Form::label('tmt_jabatan', 'Tmt Jabatan:') !!}
    <p>{!! $pegawai->tmt_jabatan !!}</p>
</div>

<!-- Tupoksi Jabatan Id Field -->
<div class="form-group">
    {!! Form::label('tupoksi_jabatan_id', 'Tupoksi Jabatan Id:') !!}
    <p>{!! $pegawai->tupoksi_jabatan_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $pegawai->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $pegawai->updated_at !!}</p>
</div>

