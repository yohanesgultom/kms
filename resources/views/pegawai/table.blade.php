<table class="table table-responsive" id="pegawai-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Username</th>
            <th>NIP/NIK</th>
            <th>Seksi Subbagian</th>
            <th>Tupoksi</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($pegawaiList as $pegawai)
        <tr>
            <td>{!! $pegawai->user->name !!}</td>
            <td>{!! $pegawai->user->username !!}</td>
            <td>{!! $pegawai->NIP_NIK !!}</td>
            <td>{!! $pegawai->seksi_subbagian !!}</td>
            <td>{!! $pegawai->tupoksi->nama_jabatan or '' !!}</td>
            <td>
                {!! Form::open(['route' => ['pegawai.destroy', $pegawai->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <div data-user-id="{{ $pegawai->user->id }}" class='btn btn-success btn-xs btn-start-chat'><i class="fa fa-comment-o" aria-hidden="true"></i></div>
                    <a href="{!! route('pegawai.show', [$pegawai->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('pegawai.edit', [$pegawai->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>