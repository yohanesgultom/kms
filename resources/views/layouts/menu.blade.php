<li class="{{ Request::is('experts*') ? 'active' : '' }}">
    <a href="{!! route('experts.index') !!}"><i class="fa fa-users"></i><span>Experts</span></a>
</li>

<li class="{{ Request::is('kasus*') ? 'active' : '' }}">
    <a href="{!! route('kasus.index') !!}"><i class="fa fa-edit"></i><span>Kasus</span></a>
</li>

<li class="{{ Request::is('knowledgeGaps*') ? 'active' : '' }}">
    <a href="{!! route('knowledgeGaps.index') !!}"><i class="fa fa-edit"></i><span>Knowledge Gaps</span></a>
</li>

<li class="{{ Request::is('pegawai*') ? 'active' : '' }}">
    <a href="{!! route('pegawai.index') !!}"><i class="fa fa-users"></i><span>Pegawai</span></a>
</li>

<li class="{{ Request::is('pengetahuanOrganisasi*') ? 'active' : '' }}">
    <a href="{!! route('pengetahuanOrganisasi.index') !!}"><i class="fa fa-edit"></i><span>Pengetahuan Organisasi</span></a>
</li>

<li class="{{ Request::is('peraturanPemerintah*') ? 'active' : '' }}">
    <a href="{!! route('peraturanPemerintah.index') !!}"><i class="fa fa-edit"></i><span>Peraturan Pemerintah</span></a>
</li>

<li class="{{ Request::is('sumberPengetahuanBaru*') ? 'active' : '' }}">
    <a href="{!! route('sumberPengetahuanBaru.index') !!}"><i class="fa fa-edit"></i><span>Sumber Pengetahuan Baru</span></a>
</li>

<li class="{{ Request::is('tupoksiJabatan*') ? 'active' : '' }}">
    <a href="{!! route('tupoksiJabatan.index') !!}"><i class="fa fa-edit"></i><span>Tupoksi</span></a>
</li>

<li class="treeview {{ Request::is('rekomendasiPengetahuan*') ? 'active' : '' }}">
    <a href="#">
        <i class="fa fa-thumbs-up"></i> <span>Rekomendasi Pengetahuan</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu {{ Request::is('rekomendasiPengetahuan/peraturanPemerintah') ? 'active' : '' }}">
        <li><a href="{!! route('rekomendasiPengetahuan.peraturanPemerintah') !!}">Peraturan Pemerintah</a></li>
    </ul>
    <ul class="treeview-menu {{ Request::is('rekomendasiPengetahuan/sumberPengetahuanBaru') ? 'active' : '' }}">
        <li><a href="{!! route('rekomendasiPengetahuan.sumberPengetahuanBaru') !!}">Sumber Pengetahuan Baru</a></li>
    </ul>
    <ul class="treeview-menu {{ Request::is('rekomendasiPengetahuan/kasus') ? 'active' : '' }}">
        <li><a href="{!! route('rekomendasiPengetahuan.kasus') !!}">Kasus</a></li>
    </ul>
    <ul class="treeview-menu {{ Request::is('rekomendasiPengetahuan/experts') ? 'active' : '' }}">
        <li><a href="{!! route('rekomendasiPengetahuan.experts') !!}">Experts</a></li>
    </ul>
</li>