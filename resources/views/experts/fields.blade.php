<!-- Name -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('user[name]', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Pengetahuan Organisasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pengetahuan_organisasi_id', 'Pengetahuan Organisasi:') !!}
    {!! Form::select('pengetahuan_organisasi_id', $pengetahuanList, null, ['class' => 'form-control', 'placeholder' => 'Please Select..']) !!}
</div>

<!-- Email -->
<div class="form-group col-sm-6">
    {!! Form::label('Email', 'Email:') !!}
    {!! Form::email('user[email]', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Name -->
<div class="form-group col-sm-6">
    {!! Form::label('username', 'Username:') !!}
    {!! Form::text('user[username]', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Password -->
<div class="form-group col-sm-6">
    {!! Form::label('Password', 'Password:') !!}
    {!! Form::password('user[password]', ['class' => 'form-control']) !!}
</div>

<!-- No Telp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user[no_telp]', 'No Telp:') !!}
    {!! Form::text('user[no_telp]', null, ['class' => 'form-control']) !!}
</div>

<!-- Available Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('available_date', 'Available Date:') !!}
    {!! Form::datetimePicker('available_date', empty($expert) ? null : $expert->available_date->format($dateFormat), ['class' => 'form-control']) !!}
</div>

<!-- Keahlian Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('keahlian', 'Keahlian:') !!}
    {!! Form::textarea('keahlian', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('experts.index') !!}" class="btn btn-default">Cancel</a>
</div>
