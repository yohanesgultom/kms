<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $expert->id !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $expert->user->nama !!}</p>
</div>

<!-- Pengetahuan Organisasi Field -->
<div class="form-group">
    {!! Form::label('pengetahuan_organisasi_id', 'Pengetahuan Organisasi:') !!}
    <p>{!! $expert->pengetahuanOrganisasi->nama_pengetahuan !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $expert->user->email !!}</p>
</div>

<!-- No Telp Field -->
<div class="form-group">
    {!! Form::label('no_telp', 'No Telp:') !!}
    <p>{!! $expert->user->no_telp !!}</p>
</div>

<!-- Available Date Field -->
<div class="form-group">
    {!! Form::label('available_date', 'Available Date:') !!}
    <p>{!! $expert->available_date !!}</p>
</div>

<!-- Keahlian Field -->
<div class="form-group">
    {!! Form::label('keahlian', 'Keahlian:') !!}
    <p>{!! $expert->keahlian !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $expert->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $expert->updated_at !!}</p>
</div>

