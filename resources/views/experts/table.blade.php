<table class="table table-responsive" id="experts-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Available Date</th>
            <th>Keahlian</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($experts as $expert)
        <tr>
            <td>{!! $expert->user->name !!}</td>
            <td>{!! $expert->user->email !!}</td>
            <td>{!! $expert->available_date !!}</td>
            <td>{!! str_limit($expert->keahlian, 20) !!}</td>
            <td>
                {!! Form::open(['route' => ['experts.destroy', $expert->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <div data-user-id="{{ $expert->user->id }}" class='btn btn-success btn-xs btn-start-chat'><i class="fa fa-comment-o" aria-hidden="true"></i></div>
                    <a href="{!! route('experts.show', [$expert->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('experts.edit', [$expert->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>