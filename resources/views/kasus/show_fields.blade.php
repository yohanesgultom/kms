<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $kasus->id !!}</p>
</div>

<!-- Kasus Field -->
<div class="form-group">
    {!! Form::label('kasus', 'Kasus:') !!}
    <p>{!! $kasus->kasus !!}</p>
</div>

<!-- User Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User:') !!}
    <p>{!! $kasus->user->name !!}</p>
</div>

<!-- Pengetahuan Organisasi Field -->
<div class="form-group">
    {!! Form::label('pengetahuan_organisasi_id', 'Pengetahuan Organisasi:') !!}
    <p>{!! $kasus->pengetahuanOrganisasi->nama_pengetahuan !!}</p>
</div>

<!-- Kata Kunci Field -->
<div class="form-group">
    {!! Form::label('kata_kunci', 'Kata Kunci:') !!}
    <p>{!! $kasus->kata_kunci !!}</p>
</div>

<!-- Indikator Field -->
<div class="form-group">
    {!! Form::label('indikator', 'Indikator:') !!}
    <p>{!! $kasus->indikator !!}</p>
</div>

<!-- Solusi Field -->
<div class="form-group">
    {!! Form::label('solusi', 'Solusi:') !!}
    <p>{!! $kasus->solusi !!}</p>
</div>

<!-- Alat Field -->
<div class="form-group">
    {!! Form::label('alat', 'Alat:') !!}
    <p>{!! $kasus->alat !!}</p>
</div>

<!-- Upload Foto Field -->
<div class="form-group">
    {!! Form::label('upload_foto', 'Upload Foto:') !!}
    @if (!empty($kasus) && !empty($kasus->upload_dokumen))
    <p><img src="data:image/png;base64,{!! base64_encode($kasus->upload_foto) !!}" height="200" /></p>
    @endif
</div>

<!-- Upload Dokumen Field -->
<div class="form-group">
    {!! Form::label('upload_dokumen', 'Upload Dokumen:') !!}
    @if (!empty($kasus) && !empty($kasus->upload_dokumen))
    <p><embed src="data:application/pdf;base64,{!! base64_encode($kasus->upload_dokumen) !!}" width="500" height="375" type='application/pdf'></p>
    @endif
</div>

<!-- Upload Video Field -->
<div class="form-group">
    {!! Form::label('upload_video', 'Upload Video:') !!}
    @if (!empty($kasus) && !empty($kasus->upload_video))
    <p><video controls><source type="video/mp4" src="data:video/mp4;base64,{!! base64_encode($kasus->upload_video) !!}"></video></p>
    @endif
</div>

<!-- Upload Audio Field -->
<div class="form-group">
    {!! Form::label('upload_audio', 'Upload Audio:') !!}    
    <p><audio controls src="data:audio/mp3;base64,{!! base64_encode($kasus->upload_audio) !!}" /></p>
</div>

<!-- Feedback Field -->
<div class="form-group">
    {!! Form::label('feedback', 'Feedback:') !!}
    <p>{!! $kasus->feedback !!}</p>
</div>

<!-- Rate Field -->
<div class="form-group">
    {!! Form::label('rate', 'Rate:') !!}
    <p>{!! $kasus->rate !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created By:') !!}
    <p>{!! $kasus->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $kasus->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $kasus->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $kasus->updated_at !!}</p>
</div>
