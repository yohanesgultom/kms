<table class="table table-responsive" id="kasus-table">
    <thead>
        <tr>
            <th>Kasus</th>
            <th>Created By</th>
            <th>Created At</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($kasus as $kasus)
        <tr>
            <td>{!! $kasus->kasus !!}</td>
            <td>{!! $kasus->created_by !!}</td>
            <td>{!! $kasus->created_at !!}</td>
            <td>
                {!! Form::open(['route' => ['kasus.destroy', $kasus->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('kasus.show', [$kasus->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('kasus.edit', [$kasus->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>