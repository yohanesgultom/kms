<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $sumberPengetahuanBaru->id !!}</p>
</div>

<!-- User Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User:') !!}
    <p>{!! $sumberPengetahuanBaru->user->name !!}</p>
</div>

<!-- Pengetahuan Organisasi Field -->
<div class="form-group">
    {!! Form::label('pengetahuan_organisasi_id', 'Pengetahuan Organisasi:') !!}
    <p>{!! $sumberPengetahuanBaru->pengetahuanOrganisasi->nama_pengetahuan !!}</p>
</div>

<!-- Jenis Field -->
<div class="form-group">
    {!! Form::label('jenis', 'Jenis:') !!}
    <p>{!! \App\Models\SumberPengetahuanBaru::JENIS[$sumberPengetahuanBaru->jenis] !!}</p>
</div>

<!-- Subject Field -->
<div class="form-group">
    {!! Form::label('subject', 'Subject:') !!}
    <p>{!! $sumberPengetahuanBaru->subject !!}</p>
</div>

<!-- Kata Kunci Field -->
<div class="form-group">
    {!! Form::label('kata_kunci', 'Kata Kunci:') !!}
    <p>{!! $sumberPengetahuanBaru->kata_kunci !!}</p>
</div>

<!-- Deskripsi Field -->
<div class="form-group">
    {!! Form::label('deskripsi', 'Deskripsi:') !!}
    <p>{!! $sumberPengetahuanBaru->deskripsi !!}</p>
</div>

<!-- Upload Foto Field -->
<div class="form-group">
    {!! Form::label('upload_foto', 'Upload Foto:') !!}
    @if (!empty($sumberPengetahuanBaru) && !empty($sumberPengetahuanBaru->upload_dokumen))
    <p><img src="data:image/png;base64,{!! base64_encode($sumberPengetahuanBaru->upload_foto) !!}" height="200" /></p>
    @endif
</div>

<!-- Upload Dokumen Field -->
<div class="form-group">
    {!! Form::label('upload_dokumen', 'Upload Dokumen:') !!}
    @if (!empty($sumberPengetahuanBaru) && !empty($sumberPengetahuanBaru->upload_dokumen))
    <p><embed src="data:application/pdf;base64,{!! base64_encode($sumberPengetahuanBaru->upload_dokumen) !!}" width="500" height="375" type='application/pdf'></p>
    @endif
</div>

<!-- Upload Video Field -->
<div class="form-group">
    {!! Form::label('upload_video', 'Upload Video:') !!}
    @if (!empty($sumberPengetahuanBaru) && !empty($sumberPengetahuanBaru->upload_video))
    <p><video controls><source type="video/mp4" src="data:video/mp4;base64,{!! base64_encode($sumberPengetahuanBaru->upload_video) !!}"></video></p>
    @endif
</div>

<!-- Upload Audio Field -->
<div class="form-group">
    {!! Form::label('upload_audio', 'Upload Audio:') !!}    
    <p><audio controls src="data:audio/mp3;base64,{!! base64_encode($sumberPengetahuanBaru->upload_audio) !!}" /></p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created By:') !!}
    <p>{!! $sumberPengetahuanBaru->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sumberPengetahuanBaru->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $sumberPengetahuanBaru->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sumberPengetahuanBaru->updated_at !!}</p>
</div>

