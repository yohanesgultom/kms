@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Sumber Pengetahuan Baru
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($sumberPengetahuanBaru, ['route' => ['sumberPengetahuanBaru.update', $sumberPengetahuanBaru->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                        @include('sumber_pengetahuan_baru.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection