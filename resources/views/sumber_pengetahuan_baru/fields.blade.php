<!-- Subject Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subject', 'Subject:') !!}
    {!! Form::text('subject', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis', 'Jenis:') !!}
    {!! Form::select('jenis', \App\Models\SumberPengetahuanBaru::JENIS, null, ['class' => 'form-control']) !!}
</div>

<!-- Pengetahuan Organisasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pengetahuan_organisasi_id', 'Pengetahuan Organisasi:') !!}
    {!! Form::select('pengetahuan_organisasi_id', $pengetahuanList, null, ['class' => 'form-control']) !!}
</div>

<!-- User Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User:') !!}
    {!! Form::select('user_id', $userList, null, ['class' => 'form-control']) !!}
</div>

<!-- Kata Kunci Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kata_kunci', 'Kata Kunci:') !!}
    {!! Form::text('kata_kunci', null, ['class' => 'form-control']) !!}
</div>

<!-- Deskripsi Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('deskripsi', 'Deskripsi:') !!}
    {!! Form::textarea('deskripsi', null, ['class' => 'form-control']) !!}
</div>

<!-- Upload Dokumen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('upload_dokumen', 'Upload Dokumen:') !!}
    {!! Form::file('upload_dokumen') !!}
</div>
<div class="clearfix"></div>

<!-- Upload Foto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('upload_foto', 'Upload Foto:') !!}
    {!! Form::file('upload_foto') !!}
</div>
<div class="clearfix"></div>

<!-- Upload Video Field -->
<div class="form-group col-sm-6">
    {!! Form::label('upload_video', 'Upload Video:') !!}
    {!! Form::file('upload_video') !!}
</div>
<div class="clearfix"></div>

<!-- Upload Audio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('upload_audio', 'Upload Audio:') !!}
    {!! Form::file('upload_audio') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('sumberPengetahuanBaru.index') !!}" class="btn btn-default">Cancel</a>
</div>
