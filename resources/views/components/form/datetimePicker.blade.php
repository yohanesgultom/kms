<div class='input-group datetime'>
    {{ Form::text($name, $value, array_merge(['class' => 'form-control'], $attributes)) }}
    <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
    </span>
</div>