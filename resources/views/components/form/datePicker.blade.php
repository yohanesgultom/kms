<div class='input-group date'>
    {{ Form::text($name, $value, array_merge(['class' => 'form-control'], $attributes)) }}
    <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
    </span>
</div>