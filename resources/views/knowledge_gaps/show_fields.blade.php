<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $knowledgeGap->id !!}</p>
</div>

<!-- Pegawai Id Field -->
<div class="form-group">
    {!! Form::label('pegawai_id', 'Pegawai Id:') !!}
    <p>{!! $knowledgeGap->pegawai_id !!}</p>
</div>

<!-- Pengetahuan Organisasi Id Field -->
<div class="form-group">
    {!! Form::label('pengetahuan_organisasi_id', 'Pengetahuan Organisasi Id:') !!}
    <p>{!! $knowledgeGap->pengetahuan_organisasi_id !!}</p>
</div>

<!-- Kepentingan Field -->
<div class="form-group">
    {!! Form::label('kepentingan', 'Kepentingan:') !!}
    <p>{!! $knowledgeGap->kepentingan !!}</p>
</div>

<!-- Pemahaman Field -->
<div class="form-group">
    {!! Form::label('pemahaman', 'Pemahaman:') !!}
    <p>{!! $knowledgeGap->pemahaman !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $knowledgeGap->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $knowledgeGap->updated_at !!}</p>
</div>

