<table class="table table-responsive" id="knowledgeGaps-table">
    <thead>
        <tr>
            <th>Pegawai</th>
            <th>Pengetahuan</th>
            <th>Kepentingan</th>
            <th>Pemahaman</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($knowledgeGaps as $knowledgeGap)
        <tr>
            <td>{!! $knowledgeGap->pegawai->user->name !!}</td>
            <td>{!! $knowledgeGap->pengetahuanOrganisasi->nama_pengetahuan !!}</td>
            <td>{!! $knowledgeGap->kepentingan !!}</td>
            <td>{!! $knowledgeGap->pemahaman !!}</td>
            <td>
                {!! Form::open(['route' => ['knowledgeGaps.destroy', $knowledgeGap->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('knowledgeGaps.show', [$knowledgeGap->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('knowledgeGaps.edit', [$knowledgeGap->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>