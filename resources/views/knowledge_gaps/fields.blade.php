<!-- Pegawai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pegawai_id', 'Pegawai:') !!}
    {!! Form::select('pegawai_id', $pegawaiList, null, ['class' => 'form-control']) !!}
</div>

<!-- Pengetahuan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pengetahuan_organisasi_id', 'Pegawai:') !!}
    {!! Form::select('pengetahuan_organisasi_id', $pengetahuanList, null, ['class' => 'form-control']) !!}
</div>

<!-- Kepentingan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kepentingan', 'Kepentingan:') !!}
    {!! Form::text('kepentingan', null, ['class' => 'form-control']) !!}
</div>

<!-- Pemahaman Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pemahaman', 'Pemahaman:') !!}
    {!! Form::text('pemahaman', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('knowledgeGaps.index') !!}" class="btn btn-default">Cancel</a>
</div>
