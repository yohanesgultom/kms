/**
 * Simple AJAX chatbox
 * 
 * Expected CSRF Meta Tag name: csrf-token
 * 
 * Expected API:
 * - Init chat session POST '/chat/start' BODY: {user_id:[userId]} RETURN: {data:{sender:Object, session:Object,recipients:[Object],messages:[Object]}}
 * - Send message POST /chat/send/${chatbox.id} BODY: {message:msg} RETURN: {data: Object}
 * - Get latest message POST /chat/sync/${chatbox.id}?last_message_id=${chatbox.lastMessageId} RETURN: {data: [Object]}
 * 
 * TODO:
 * - Load user pic from API
 * - Dynamically configure CSRF & URL 
 * 
 */

var chatbox = {

    html: `
<div class="chatbox">
    <div class="chatbox__title">
        <h5><a href="#"></a></h5>
        <button class="chatbox__title__tray">
            <span></span>
        </button>
        <button class="chatbox__title__close">
            <span>
                <svg viewBox="0 0 12 12" width="12px" height="12px">
                    <line stroke="#FFFFFF" x1="11.75" y1="0.25" x2="0.25" y2="11.75"></line>
                    <line stroke="#FFFFFF" x1="11.75" y1="11.75" x2="0.25" y2="0.25"></line>
                </svg>
            </span>
        </button>
    </div>
    <div class="chatbox__body">
    </div>
    <textarea class="chatbox__message" placeholder="Write something interesting"></textarea>
</div>`,

    getContainer: function () { return $('.chatbox'); },
    getTitle: function () { return $('.chatbox__title'); },
    getBody: function () { return $('.chatbox__body'); },
    getClose: function () { return $('.chatbox__title__close'); },
    getInput: function () { return $('.chatbox__message'); },
    getMessage: function () { return $('.chatbox__message'); },

    id: null,
    recipients: [],
    lastMessageId: null,
    interval: null,

    init: function (id, user, recipients, messages) {
        this.id = id;
        this.sender = user,
            this.recipients = {};
        recipients.forEach(user => {
            this.recipients[user.id] = user;
        });
        this.messages = messages;
        this.lastMessageId = messages.length > 0 ? messages[messages.length - 1].id : undefined;
        // append to body
        $('body').append(this.html);
        // set id to container
        this.getContainer().attr('id', id);
        // concat recipients name as title
        let title = Object.values(this.recipients).map(u => u.name);
        this.getTitle().find('a').text(title.join(', '));
        // clear and add messages
        this.getBody().empty();
        this.displayMessages(messages);
        this.show();
        this.initHandlers();
        // set sync interval
        this.interval = setInterval(this.sync, 1000);
    },

    initHandlers: function () {

        this.getTitle().on('click', function () {
            chatbox.getContainer().toggleClass('chatbox--tray');
        });

        this.getClose().on('click', function (e) {
            e.stopPropagation();
            chatbox.getContainer().addClass('chatbox--closed');
        });

        this.getContainer().on('transitionend', function () {
            let container = chatbox.getContainer();
            if (container.hasClass('chatbox--closed')) {
                chatbox.getContainer().remove();
                clearInterval(chatbox.interval);
            }
        });

        this.getMessage().on('keypress', function (e) {
            if (e.which == 13) {
                e.preventDefault();
                let msg = $(this).val().trim();
                if (!msg) return false;
                let msgDiv = `
<div class="chatbox__body__message chatbox__body__message--right">
    <img src="/images/anonymous.png" alt="Picture">
    <p>${msg}</p>
</div>`;
                $.ajax({
                    url: `/chat/send/${chatbox.id}`,
                    type: 'post',
                    data: { message: msg },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json'
                })
                    .done(function (res) {
                        let body = chatbox.getBody();
                        let messageBox = chatbox.getMessage();
                        chatbox.lastMessageId = res.data.id;
                        body.append(msgDiv).scrollTop(body.prop('scrollHeight'));
                        messageBox.val('');
                    })
                    .fail(function (err) {
                        alert('Message not sent. Please retry');
                    });
            }
        });
    },

    hide: function () {
        this.getContainer().addClass('hidden').addClass('chatbox--tray');
    },

    show: function () {
        this.getContainer().removeClass('hidden').removeClass('chatbox--tray');
    },

    displayMessages: function (messages) {
        messages = messages || [];
        let body = this.getBody();
        for (let i = 0; i < messages.length; i++) {
            let msg = messages[i];
            let user = this.sender;
            let position = 'right';
            // TODO: load image from user profile
            let img = '/images/anonymous.png';
            if (msg.user_id != this.sender.id) {
                user = this.recipients[msg.user_id];
                position = 'left';
                img = '/images/anonymous.png';
            }
            let msgDiv = `
<div class="chatbox__body__message chatbox__body__message--${position}">
    <img src="${img}" alt="${user.name}">
    <p>${msg.message}</p>
</div>`;
            body.append(msgDiv);
        }
        body.scrollTop(body.prop('scrollHeight'));
    },

    sync: function () {
        $.ajax({
            url: `/chat/sync/${chatbox.id}?last_message_id=${chatbox.lastMessageId}`,
            type: 'get',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json'
        })
            .done(function (res) {
                let messages = res.data;
                if (messages.length > 0) {
                    chatbox.lastMessageId = messages[messages.length - 1].id;
                    chatbox.displayMessages(res.data);
                }                
            })
            .fail(console.error);
    },

    count: function(cb) {
        $.ajax({
            url: '/chat/count',
            type: 'get',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json'
        })
            .done(function (res) {
                cb(res.data);
            })
            .fail(console.error);        
    }
}