$(document).ready(function() {

    // init datetimepicker input
    $('.datetime').datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
    });

    // init datepicker input
    $('.date').datetimepicker({
        format: 'DD/MM/YYYY',
    });


    // load last chats
    setInterval(function() {
        chatbox.count(function(data) {            
            let messageMenu = $('.messages-menu ul.messages');
            let notification = $('.messages-menu .notification');            
            let notificationCount = data.reduce(function (sum, d) { return sum + d.message_count; }, 0);

            // hide/show notification
            if (notificationCount <= 0) {
                notification.text('').addClass('hidden');                
            } else {
                notification.text(notificationCount).removeClass('hidden');
            }

            // update messages list
            if (notification.children().length <= 0 
                || notificationCount.toString() == notification.text()) {
                
                // append message
                messageMenu.empty();
                for (let i = 0; i < data.length; i++) {
                    let info = data[i];
                    let sender = info.chat.users
                        .filter(u => u.id != info.user.id)
                        .map(u => u.name)
                        .join(', ');
                    
                    let html = `
<li>
    <a class="btn-load-chat" data-chat-id="${info.chat_session_id}" href="javascript:">
        <div class="pull-left">
            <img src="/images/anonymous.png" class="img-circle" alt="${info.user.name}">
        </div>
        <h4>${sender} <span class="label label-danger ${info.message_count <= 0 ? 'hidden' : ''}">${info.message_count}</span></h4>
        <p></p>
    </a>
</li>`;
                    messageMenu.append($(html));
                }

                // add handler
                $('.btn-load-chat').on('click', function (e) {
                    e.preventDefault();
                    let chatId = $(this).data('chat-id');
                    $.ajax({
                        url: `/chat/load/${chatId}`,
                        type: 'get',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json'
                    })
                        .done(function (res) {
                            let data = res.data;
                            chatbox.init(data.session.id, data.sender, data.recipients, data.messages);
                        })
                        .fail(console.error);
                });            
            }

        });
    }, 5000);

    // chat triggers
    $('.btn-start-chat').on('click', function (e) {
        e.preventDefault();
        let userId = $(this).data('user-id');
        let data = { user_id: [userId] };
        $.ajax({
            url: '/chat/start',
            type: 'post',
            data: {
                user_id: [userId]
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json'
        })
            .done(function (res) {
                let data = res.data;
                chatbox.init(data.session.id, data.sender, data.recipients, data.messages);
            })
            .fail(console.error);
    });
    
});