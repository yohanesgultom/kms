<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTupoksiJabatanTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tupoksi_jabatan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_jabatan');
            $table->text('ikhtisar_jabatan')->nullable();
            $table->text('uraian_tugas')->nullable();
            $table->text('hasil_kerja')->nullable();
            $table->text('tanggung_jawab')->nullable();
            $table->text('syarat_jabatan')->nullable();
            $table->date('tanggal')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();            
            $table->timestamps();
            $table->softDeletes();
        });

        \DB::statement("ALTER TABLE tupoksi_jabatan ADD upload_dokumen MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tupoksi_jabatan');
    }
}
