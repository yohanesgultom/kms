<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeraturanPemerintahTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peraturan_pemerintah', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('jenis')->nullable();
            $table->string('perihal');
            $table->date('tanggal')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->integer('pengetahuan_organisasi_id')->unsigned();
            $table->foreign('pengetahuan_organisasi_id')->references('id')->on('pengetahuan_organisasi');            
            $table->timestamps();
            $table->softDeletes();
            $table->index('perihal');
        });

        \DB::statement("ALTER TABLE peraturan_pemerintah ADD upload_dokumen MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peraturan_pemerintah');
    }
}
