<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSumberPengetahuanBaruTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sumber_pengetahuan_baru', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jenis');
            $table->string('subject');
            $table->string('kata_kunci')->nullable();
            $table->text('deskripsi')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();            
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');            
            $table->integer('pengetahuan_organisasi_id')->unsigned();
            $table->foreign('pengetahuan_organisasi_id')->references('id')->on('pengetahuan_organisasi');            
            $table->timestamps();
            $table->softDeletes();
            $table->index('subject');
        });

        \DB::statement("ALTER TABLE sumber_pengetahuan_baru ADD upload_foto MEDIUMBLOB");
        \DB::statement("ALTER TABLE sumber_pengetahuan_baru ADD upload_dokumen MEDIUMBLOB");
        \DB::statement("ALTER TABLE sumber_pengetahuan_baru ADD upload_video MEDIUMBLOB");
        \DB::statement("ALTER TABLE sumber_pengetahuan_baru ADD upload_audio MEDIUMBLOB");        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sumber_pengetahuan_baru');
    }
}
