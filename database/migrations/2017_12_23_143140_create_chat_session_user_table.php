<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatSessionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_session_user', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('message_count')->default(0);
            $table->integer('chat_session_id')->unsigned();
            $table->foreign('chat_session_id')->references('id')->on('chat_sessions');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_session_user');
    }
}
