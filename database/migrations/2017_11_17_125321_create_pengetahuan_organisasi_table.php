<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePengetahuanOrganisasiTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengetahuan_organisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pengetahuan');
            $table->text('deskripsi_pengetahuan')->nullable();
            $table->date('tanggal')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();            
            $table->timestamps();
            $table->softDeletes();
            $table->index('nama_pengetahuan');
        });

        \DB::statement("ALTER TABLE pengetahuan_organisasi ADD upload_dokumen MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pengetahuan_organisasi');
    }
}
