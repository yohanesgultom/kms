<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKasusTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kasus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kasus');
            $table->string('kata_kunci')->nullable();
            $table->string('indikator')->nullable();
            $table->text('solusi')->nullable();
            $table->string('alat')->nullable();
            $table->text('feedback')->nullable();
            $table->integer('rate')->default(0);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();            
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('pengetahuan_organisasi_id')->unsigned();
            $table->foreign('pengetahuan_organisasi_id')->references('id')->on('pengetahuan_organisasi');
            $table->timestamps();
            $table->softDeletes();
            $table->index('kasus');
        });

        \DB::statement("ALTER TABLE kasus ADD upload_foto MEDIUMBLOB");
        \DB::statement("ALTER TABLE kasus ADD upload_dokumen MEDIUMBLOB");
        \DB::statement("ALTER TABLE kasus ADD upload_video MEDIUMBLOB");
        \DB::statement("ALTER TABLE kasus ADD upload_audio MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kasus');
    }
}
