<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKnowledgeGapsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowledge_gaps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kepentingan')->default(0);
            $table->integer('pemahaman')->default(0);
            $table->integer('pegawai_id')->unsigned();
            $table->foreign('pegawai_id')->references('id')->on('pegawai');            
            $table->integer('pengetahuan_organisasi_id')->unsigned();
            $table->foreign('pengetahuan_organisasi_id')->references('id')->on('pengetahuan_organisasi');            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('knowledge_gaps');
    }
}
