<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePegawaiTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->increments('id');
            $table->string('NIP_NIK');
            $table->string('seksi_subbagian')->nullable();
            $table->string('tmt_jabatan')->nullable();
            $table->integer('tupoksi_jabatan_id')->unsigned()->nullable();
            $table->foreign('tupoksi_jabatan_id')->references('id')->on('tupoksi_jabatan');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pegawai');
    }
}
