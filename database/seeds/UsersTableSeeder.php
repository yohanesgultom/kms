<?php

use Illuminate\Database\Seeder;
use \App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'administrator',
          'username' => 'admin',
          'role' => User::ROLES['ADMINISTRATOR'],
          'email' => 'admin@email.com',
          'password' => bcrypt('admin'),
          'created_at' => new \DateTime()
      ]);
    }
}
