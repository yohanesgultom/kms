<?php

use Illuminate\Database\Seeder;

class PegawaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $faker = Faker\Factory::create();
        $tupoksiJabatanIds = \App\Models\TupoksiJabatan::pluck('id')->all();        
        $pegawaiList = factory(App\Models\Pegawai::class, 10)->make();
        foreach($pegawaiList as $pegawai) {
            $pegawai->tupoksi_jabatan_id = $faker->randomElement($tupoksiJabatanIds);
            $pegawai->save();
        }
    }
}
