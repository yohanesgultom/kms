<?php

use Illuminate\Database\Seeder;

class KnowledgeGapTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $pegawaiIds = \App\Models\Pegawai::pluck('id')->all();
        $pengetahuanIds = \App\Models\PengetahuanOrganisasi::pluck('id')->all();
        $max = min(count($pengetahuanIds), 10);
        foreach($pegawaiIds as $pegawaiId) {
            $shuffled = $faker->shuffle($pengetahuanIds);
            for ($i = 0; $i < $max; $i++) {
            factory(App\Models\KnowledgeGap::class)
                ->create([
                    'pegawai_id' => $pegawaiId,
                    'pengetahuan_organisasi_id' => $shuffled[$i],
                ]);
            }
        }
    }
}
