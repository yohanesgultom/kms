<?php

use Illuminate\Database\Seeder;

class SumberPengetahuanBaruTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $pengetahuanOrganisasiIds = \App\Models\PengetahuanOrganisasi::pluck('id')->all();
        $pegawaiList = \App\Models\Pegawai::with('user')->get()->toArray();
        $pengetahuanBaruList = factory(App\Models\SumberPengetahuanBaru::class, 3)->make();
        foreach ($pengetahuanBaruList as $pengetahuanBaru) {
            $pegawai = $faker->randomElement($pegawaiList);
            $pengetahuanBaru->user_id = $pegawai['user_id'];
            $pengetahuanBaru->created_by = $pegawai['user']['username'];
            $pengetahuanBaru->pengetahuan_organisasi_id = $faker->randomElement($pengetahuanOrganisasiIds);
            $pengetahuanBaru->save();
        }

    }
}
