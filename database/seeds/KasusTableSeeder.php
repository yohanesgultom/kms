<?php

use Illuminate\Database\Seeder;

class KasusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $pengetahuanOrganisasiIds = \App\Models\PengetahuanOrganisasi::pluck('id')->all();
        $pegawaiList = \App\Models\Pegawai::with('user')->get()->toArray();
        $kasusList = factory(App\Models\Kasus::class, 3)->make();
        foreach ($kasusList as $kasus) {
            $pegawai = $faker->randomElement($pegawaiList);
            $kasus->user_id = $pegawai['user_id'];
            $kasus->created_by = $pegawai['user']['username'];
            $kasus->pengetahuan_organisasi_id = $faker->randomElement($pengetahuanOrganisasiIds);
            $kasus->save();
        }

    }
}
