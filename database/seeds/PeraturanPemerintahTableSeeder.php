<?php

use Illuminate\Database\Seeder;

class PeraturanPemerintahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $pengetahuanOrganisasiIds = \App\Models\PengetahuanOrganisasi::pluck('id')->all();
        $peraturanList = factory(App\Models\PeraturanPemerintah::class, 10)->make();
        foreach ($peraturanList as $peraturan) {
            $peraturan->pengetahuan_organisasi_id = $faker->randomElement($pengetahuanOrganisasiIds);
            $peraturan->save();
        }
    }
}
