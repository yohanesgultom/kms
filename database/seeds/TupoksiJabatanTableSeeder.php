<?php

use Illuminate\Database\Seeder;

class TupoksiJabatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\TupoksiJabatan::class, 5)->create();
    }
}
