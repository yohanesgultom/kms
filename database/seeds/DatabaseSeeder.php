<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PengetahuanOrganisasiTableSeeder::class);
        $this->call(TupoksiJabatanTableSeeder::class);
        $this->call(PegawaiTableSeeder::class);
        $this->call(ExpertsTableSeeder::class);
        $this->call(PeraturanPemerintahTableSeeder::class);
        $this->call(KnowledgeGapTableSeeder::class);
        $this->call(KasusTableSeeder::class);
        $this->call(SumberPengetahuanBaruTableSeeder::class);
    }
}
