<?php

use Illuminate\Database\Seeder;

class ExpertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $pengetahuanOrganisasiIds = \App\Models\PengetahuanOrganisasi::pluck('id')->all();        
        
        factory(App\User::class, 10)
            ->states('expert')
            ->create()
            ->each(function ($u) use (&$t, $faker, $pengetahuanOrganisasiIds) {
                factory(App\Models\Expert::class)->create([
                    'user_id' => $u->id,
                    'pengetahuan_organisasi_id' => $faker->randomElement($pengetahuanOrganisasiIds)
                ]);                
            });
    }
}
