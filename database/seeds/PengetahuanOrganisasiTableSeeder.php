<?php

use Illuminate\Database\Seeder;

class PengetahuanOrganisasiTableSeeder extends Seeder
{

    private $data = [
        [ 
            "nama_pengetahuan" =>  "Penyelenggaraan Pendidikan dan Pelatihan di organisasi BDTBT",
            "deskripsi_pengetahuan" => <<<'EOD'
Proses penyelenggaraan diklat yang baik merupakan suatu siklus yang komplek. Dimulai dengan analisa kebutuhan diklat sampai evaluasi hasil diklat. Merupakan suatu kesatuan yang utuh agar didapatkan kualitas penyelenggaraan diklat yang sempurna. Sesuai dengan Kepmen ESDM No 1453 k/29/MEM/2000 tentang Pedoman Teknis Penyelenggaraan Tugas Pemerintahan di Bidang Pertambangan Umum dan Peraturan Menteri ESDM No. 10 Tahun 2013 tentang Organisasi dan Tata Kerja Balai Diklat Tambang Bawah Tanah, guna mendukung sumber daya manusia subsektor tambang bawah tanah yang memiliki kompetensi, profesional, berdaya saing tinggi dalam rangka melaksanakan tugas pemerintahan dan pembangunan. Balai Diklat Tambang Bawah Tanah selaku organisasi pendidikan dan pelatihan khususnya di bidang tambang bawah tanah menyelenggarakan program diklat berbasis kompetensi dengan hasil (Outcame) terwujudnya pengembangan SDM berbasis kompetensi dan berbasis kebutuhan pemangku kepentingan, dengan jenis keluaran (output) Layanan pendidikan dan pelatihan.            
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Analisis Kebutuhan Diklat",
            "deskripsi_pengetahuan" => <<<'EOD'
Analisis Kebutuhan Diklat merupakan suatu rangkaian perencanaan untuk menentukan kebutuhan diklat suatu organisasi dalam dalam rangka memenuhi kebutuhan organisasinya. Hal ini tentu tidak terlepas dari kebutuhan peserta atau instansi peserta diklat, harus bisa membaca apa yang dibutuhkan oleh peserta. Maka oleh sebab itu dilakukanlah analisis kebutuhan diklat untuk mengetahui sejauhmana kebutuhan diklat yang diperlukan bagi pengguna diklat, baik bagi para apartur pemerintah yang bertugas mengelola kegiatan pertambangan di daerah, tenaga kerja di industri pertambangan maupun stakeholder lainnya. Dengan demikian diharapkan penyelenggaraan diklat nantinya dapat benar-benar sesuai kebutuhan dan dapat menjawab permasalahan perbaikan dan peningkatan kinerja bagi pengguna diklat maupun organisasi serta dapat diketahui penyebaran pangsa pasar pengguna diklat di bidang tambang bawah tanah. Pemantauan Penyelenggaraan Diklat ditujukan untuk menilai performance pelayanan diklat dari seksi penyelenggaraan dan evaluasi diklat baik kepada peserta maupun kepada Widyaiswara            
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "NSPK (Norma Standar Prosedur Kriteria) Diklat",
            "deskripsi_pengetahuan" => <<<'EOD'
Untuk meningkatkan kualitas sumber daya manusia sektor pertambangan atau bidang Energi dan Sumber Daya Mineral, dilakukan dengan berbagai cara antara lain melalui pedidikan dan pelatihan berbasis kompetensi. Komponen penting dalam suatu pelaksanaan diklat pada lembaga diklat adalah tersedianya kurikulum diklat standar. Agar perangkat diklat dapat disusun, dibutuhkan kurikulum diklat berbasis kompetensi yang sesuai dengan kebutuhan kompetensi jabatan/fungsi kerja tertentu. Oleh karena itu, untuk seseorang yang akan menduduki jabatan/melakukan pekerjaan bidang tambang bawah tanah perlu disusun rancangan kurikulum tambang bawah yang berbasis kompetensi.

Dalam suatu perencanaan program pendidikan dan pelatihan berbasis kompetensi diperlukan Standar Kompetensi Kerja (SKK) untuk setiap sektor/bidang garapan. SKK/Analisis Kompetensi yang telah distandarkan/disusun, kemudian dianalisis untuk disusun menjadi kurikulum yang terdiri dari garis-garis program pengajaran seperti mata diklat, metode, media dan fasilitas pendidikan. Disamping SKKNI, untuk sektor/ bidang garapan yang belum memiliki standar kerja tertentu maka sebuah instansi diklat dapat menyusus sebuah analisis kompetensi sebagai standar kerja yang digunakan dalam pendidikan dan pelatihan. Kurikulum yang telah tersusun tersebut menjadi pedoman dalam penysunan Modul Diklat. Modul Diklat merupakan bagian dari media pembelajaran, ketersediaannya mendukung tercapainya kegiatan belajar secara efektif dan efisien.  Kurikulum dan Modul Diklat dirancang sesuai dengan tuntutan penyusunan kurikulum berbasis kompetensi yang mengacu pada Taxonomi Bloom dimana untukmenjadi kompeten harus memiliki tiga aspek yaitu:
a. Pengetahuan/kognitif (knowledge);
b. Keterampilan/psikomotorik (skill), dan
c. Sikap/afektif (attitude) kerja.
Ketiga ranah di atas merupakan kesatuan yang tidak terpisahkan yang akan melengkapi peserta diklat dengan perangkat Diklat.

Dengan tersedianya Kurikulum, Modul, Pedoman Penyelenggaraan Diklat dan Materi Uji Kompetensi ini diharapkan akan menunjang kemampuan widyaiswara/instruktur dalam memberikan program diklat yang keseluruhannya mendukung terselenggaranya diklat yang berbasis kompetensi di Balai Diklat Tambang Bawah Tanah.

Disamping hal diatas, penyediaan Standar Operasional Prosedur sarana dan prasarana Diklat juga merupakan keharusan bagi sebuah lembaga Diklat agar penyelenggaraan Diklat dapat terlaksana sesuai dengan kebutuhan yang telah ditetapkan dalan Standar Kurikulum Diklat. Penyusunan Standar Operasional Prosedur tersebut termasuk juga penyusunan Instruksi Kerja masing-masing peralatan dan perlengkapan yang digunakan sebagai alat praktek diklat maupun alat uji kompetensi sebagai Tempat Uji Kompetensi            
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Evaluasi Kediklatan",
            "deskripsi_pengetahuan" => <<<'EOD'
Evaluasi kediklatan merupakan suatu rangkaian penyelenggaraan diklat untuk mengetahui sejauh mana diklat tersebut berdampak pada organisasi maupun peserta diklat. Hal ini tentu tidak terlepas dari hasil pendidikan dan pelatihan oleh peserta atau instansi peserta diklat. Maka oleh sebab itu dilakukanlah evaluasi kediklatan untuk mengetahui sejauhmana penyelanggaraan diklat menghasilkan manfaatnya bagi organisasi, baik bagi para apartur pemerintah yang bertugas mengelola kegiatan pertambangan di daerah, tenaga kerja di industri pertambangan maupun stakeholder lainnya. Dengan demikian diharapkan penyelenggaraan diklat nantinya dapat benar-benar digunakan di organisasi peserta diklat dan dapat menjawab permasalahan perbaikan dan peningkatan kinerja bagi pengguna diklat maupun organisasi serta dapat diketahui penyebaran pangsa pasar pengguna diklat di bidang tambang bawah tanah. Pemantauan Penyelenggaraan Diklat ditujukan untuk menilai performance pelayanan diklat dari seksi penyelenggaraan  diklat baik kepada peserta maupun kepada Widyaiswara.
Peningkatan kualitas sumber daya manusia merupakan kunci keberhasilan dalam pembangunan, hal ini dapat meningkatkan produktivitas dan efesiensi. Pembangunan sumber daya manusia diarahkan agar benar-benar mampu dan memiliki etos kerja yang produktif, kreatif serta disiplin. Untuk mengetahui peningkatan kualitas sumber daya manusia biasanya melalui kediklatan tertentu, setelah itu untuk mengetahui tingkat keberhasilan dan kebermaknaan diklat tersebut dilakukan Laporan dan Evaluasi Kediklatan
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Publikasi karya ilmiah",
            "deskripsi_pengetahuan" => <<<'EOD'
Kegiatan pendidikan dan pelatihan adalah salah satu kegiatan pelayanan jasa yang perlu sekali diperhatikan proses penyelenggaraannya. Baik dari sisi materi yang diajarkan, pengajar maupun kualitas penyelenggarannya sendiri. Dengan melalui penelitian terutama bidang kediklatan sesuai dengan bidang teknis yang dikuasai, diharapkan widyaiswara dapat lebih menguasai bidang ilmu yang diampu. Untuk itu diperlukan sekali suatu tim yang dapat melakukan penelitian dan pengembangan dari materi diklat seperti pedoman praktek lapangan, lokasi praktek, materi yang akan diajarkan dalam diklat dan lain-lain. Sehingga kualitas dan keprimaan suatu diklat dari waktu ke waktu dapat ditingkatkan.

Hasil dari penelitian karya ilmiah ini patut dipublikasikan ke kelayakan masyarakat tambang, karena Balai Diklat Tambang Bawah Tanah sebagai satu-satunya institusi pemerintah yang menciptakan sumber daya manusia bidang tambang bawah tanah punya juga andil dalam memajukan pertambangan di Indonesia. Dengan publikasi karya ilmiah ini diharapkan dapat menjadi menambah wawasan pengetahuan untuk masyarakat pertambangan di Indonesia khususnya pertambangan bawah tanah
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Penulisan Karya Ilmiah",
            "deskripsi_pengetahuan" => <<<'EOD'
Penelitian yang merupakan kegiatan ilmiah dan terencana bertujuan untuk membuktikan keberadaan dan keabsahan suatu obyek, pendapat atau masalah. Menurut Kerlinger (2002), definisi penelitian ilmiah adalah penyelidikan yang sistematis, terkontrol, empiris dan kritis tentang fenomena-fenomena alami, dengan dipandu oleh teori dan hipotesis-hipotesis mengenai hubungan yang diperkirakan terdapat di antara fenomena-fenomena tersebut. Sedangkan kegiatan ilmiah berarti kegiatan yang dilandasi oleh metode keilmuan. Lebih spesifik Prof. Sugiyono (2006) menjelaskan tentang metode penelitian pendidikan adalah cara ilmiah untuk mendapatkan data yang valid dengan tujuan untuk dapat ditemukan, dikembangkan dan dibuktikan suatu pengetahuan tertentu sehingga pada gilirannya dapat digunakan untuk memahami, memecahkan dan mengantisipasi masalah dalam bidang pendidikan. Guna mendukung terwujudnya sumber daya manusia subsektor tambang bawah tanah yang memiliki kompetensi, profesional, berdaya saing tinggi dalam rangka melaksanakan tugas pendidikan dan pelatihan. Balai Diklat Tambang Bawah Tanah menyelenggarakan 7 penelitian yang mencakup lima bidang yaitu listrik, mesin, tambang, lingkungan, dan keselamatan tambang.
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Sarana dan Prasarana yang sesuai dengan Kebutuhan",
            "deskripsi_pengetahuan" => <<<'EOD'
Saat ini, metode penambangan bawah tanah sudah sudah banyak diaplikasikan di Indonesia. Seiring dengan kemajuan teknologi, tingkat keselamatan pekerja di tambang bawah tanah juga sudah semakin baik. Dengan banyaknya pembukaan tambang-tambang bawah tanah ini maka kesiapan aparatur pemerintah juga dituntut siap guna memberikan izin, membina serta mengawasi kegiatan ini agar dilaksanakan dengan menerapkan prinsip-prinsip penambangan yang baik dan benar. Disamping itu, tenaga-tenaga terampil sebagai operator tambang ini, juga harus mendapatkan pendidikan dan pelatihan yang baik pula. Sehingga industri penambangan yang ada di suatu daerah di Indonesia akan memberikan manfaat yang lebih banyak ketimbang kerugian akibat tidak diterapkannya penambangan yang baik dan benar. Melihat dari perkembangan tambang bawah tanah di Indonesia saat ini, metode penambangan bawah tanah saat ini sudah banyak yang diaplikasikan pada tambang-tambang mineral logam. Maka sudah seharusnya BDTBT sebagai institusi satu-satunya dalam bidang diklat tambang bawah tanah mengembangkan diklat-diklat khusus dibidang tambang bawah tanah untuk mineral agar dapat mencapai visi dan misinya untuk menjadi institusi terunggul dalam menciptakan tenaga terampil di bidang tambang bawah Tanah Sarana dan Prasarana yang Sesuai Roadmap/kebutuhan, untuk mewujudkan  visi misi tersebut adalah Pengadaan Peralatan Sarana dan prasarana kediklatan.
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Dokumen Perencanaan",
            "deskripsi_pengetahuan" => <<<'EOD'
Untuk meningkatkan efisiensi dan efektifitas program pendidikan dan pelatihan, serta agar tetap eksis dan unggul menghadapi persaingan yang semakin ketat dalam lingkungan yang berubah sangat cepat, maka Balai Diklat Tambang Bawah Tanah secara terus menerus harus melakukan perubahan kearah perbaikan. Perubahan tersebut perlu disusun dalam suatu perencanaan yang konsisten, berkelanjutan, akuntabel, dan berorientasi kepada pencapaian kinerja. Perencanaan stratejik yang telah ditetapkan sebagai pedoman bagi Balai Diklat Tambang Bawah Tanah untuk mencapai dan bagaimana cara mencapai tujuan organisasi perlu terus dikaji dan disesuaikan dengan tuntutan dan perubahan.

Program/kegiatan Balai Diklat Tambang Bawah Tanah harus sesuai dengan kebijakan nasional dan diarahkan untuk mendukung pencapaian sasaran Balai Diklat Tambang Bawah Tanah, sasaran Badiklat, sasaran Kementerian dan sasaran Pembangunan secara nasional. agar program dan kegiatan di atas dapat dilaksanakan pada tataran operasional perlu didukung oleh anggaran yang realistis dan memadai.
Suatu organisasi dapat mencapai suatu tujuannya apabila dapat merumuskan suatu perencanaan yang baik dalam bentuk program kegiatan, perencanaan yang baik tidak akan lepas dari prinsip-prinsip akuntabel, fleksibel dan berkesinambungan, terkait dengan usulan kegiatan/anggaran yang realistis dan dikelola secara profesional dan terpadu di Balai Diklat Tambang Bawah Tanah.

Perencanaan yang ada di Balai Diklat Tambang Bawah Tanah harus pararel dengan target pelaksanaannya sehingga tujuan umum dari pemerintah tercapai yaitu memajukan perekonomian bangsa dan Negara. Dalam hal ini terwujud dalam performa kinerja kelembagaan sehingga hal ini tidak kalah pentingnya untuk menjadi perhatian dari tujuan secara keseluruhan. Kinerja kelembagaan menjadi ukuran yang pasti bahwa perencanaan yang di susun menjadi tepat sasaran. Penyusunan Dokumen Perencanaan Anggaran RKAKL merupakan dokumen yang disusun berdasarkan pagu sementara dan definitif sebagai konsep untuk membuat DIPA.
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Layanan Perkantoran",
            "deskripsi_pengetahuan" => <<<'EOD'
Balai Pendidikan dan Pelatihan Tambang Bawah Tanah mempunyai tugas teknis sebagai penunjang dan operasional bidang Pendidikan dan Pelatihan Tambang Bawah Tanah. Dalam rangka menunjang kelancaran pelaksanaan tugas organisasi Balai Pendidikan dan Pelatihan Tambang Bawah Tanah sebagai dasar untuk meningkatkan pelayanan untuk mewujudkan tata pemerintahan yang baik, diperlukan dukungan ketersediaan anggaran yang harus berupa kewajiban kantor dalam penatausahaan dan pembayaran gaji dan tunjangan pegawai secara teratur setiap bulan sesuai kebutuhan Balai Pendidikan dan Pelatihan Tambang Bawah Tanah serta diperlukan dukungan operasional perkantoran sesuai kebutuhan dalam menunjang pelaksanaan tugas dan fungsi. 

Balai Pendidikan dan Pelatihan Tambang Bawah Tanah melaksanakan pemenuhan kebutuhan layanan perkantoran setiap tahunnya, meliputi :  
a. Pembayaran Gaji dan Tunjangan
b. Penyelenggaraan Operasional & Pemeliharaan kantor

Penyelenggaraan Operasional & pemeliharaan kantor ini antara lain, yaitu :
o Pemeliharaan Kebersihan
o Pemeliharaan Keamanan dan Layanan Umum
o Langganan Daya dan Jasa
o Jasa Pos dan Giro
o Poliklinik & Obat-obatan
o Rapat Koordinasi
o Sewa Jaringan Telekomunikasi (Leased Channel) Satelit
o Perawatan Kendaraan Bermotor Roda 2/4/6/10
o Perawatan Peralatan Kantor
o Pemeliharan Peralatan Sarana Kediklatan
o Perawatan Perangkat Teknologi Informasi dan Komunikasi
o Pemeliharaan Gedung dan Bangunan Lainnya
o Sewa Rumah Dinas Pegawai Negeri  Sipil
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Pengelolaan Manajemen Keuangan, Aset dan Kinerja",
            "deskripsi_pengetahuan" => <<<'EOD'
Sub Bagian Tata Usaha mempunyai tugas teknis penunjang dan operasional melaksanakan bidang pendidikan dan pelatihan Tambang Bawah Tanah. Dalam rangka menunjang kelancaran pelaksanaan tugas organisasi Balai Pendidikan dan Pelatihan Tambang Bawah Tanah sebagai dasar untuk meningkatkan pelayanan untuk mewujudkan tata pemerintahan yang baik, diperlukan suatu dukungan penyelenggaraan kegiatan administrasi baik dari segi pengelolaan manajemen keuangan, aset  dan kinerja yang tertib dan akuntabel, sehingga dapat menunjang terselenggaranya tugas pokok dan fungsi Balai Diklat Tambang Bawah Tanah. Kegiatan Pengelolaan Manajemen Keuangan, Aset dan Kinerja meliputi :
a. Sistem Akuntansi Barang Milik/Kekayaan Negara
b. Penghapusan Barang Milik/Kekayaan Negara
c. Penyusunan Laporan Keuangan Semesteran dan Tahunan 
d. Penyusunan Laporan Akuntabilitas Kinerja Instansi Pemerintah
e. Penyusunan Laporan Tahunan
f. Penyusunan Laporan Triwulan
g. Pengelolaan Pendapatan Negara Bukan Pajak (PNBP)
h. Rekonsiliasi Realisasi Fisik dan Anggaran
i. Sensus Barang Milik/Kekayaan Negara
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Administrasi Ketatausahaan",
            "deskripsi_pengetahuan" => <<<'EOD'
Dalam rangka menunjang kelancaran pelaksanaan tugas organisasi Balai Pendidikan dan Pelatihan Tambang Bawah Tanah sebagai dasar untuk meningkatkan pelayanan untuk mewujudkan tata pemerintahan yang baik, diperlukan suatu dukungan penyelenggaraan kegiatan administrasi baik dari segi pengelolaan administrasi ketatausahaan dan umum yang tertib dan akuntabel, sehingga dapat menunjang terselenggaranya tugas pokok dan fungsi Balai Diklat Tambang Bawah Tanah. Kegiatan Pengelolaan/Pembinaan Administrasi Kepegawaian, Umum dan keuangan meliputi:
a. Gugus Tugas Hemat Energi
b. Pengelolaan Persediaan Kantor 
c. Pengelolaan dan Revisi Anggaran
d. Koordinasi Pelaksanaan Anggaran/Undangan/Penyelenggaraan Kegiatan
e. Operasional ULP dan Sekretariat Pejabat Pembuat Komitmen (P2K)
f. Kegiatan Operasional Pengelolaan Sarana dan Prasarana Kediklatan
g. Monitoring, Evaluasi Pelaksanaan Kegiatan dan Anggaran
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "administrasi kepegawaian",
            "deskripsi_pengetahuan" => <<<'EOD'
Pelaksanaan tugas organisasi Balai Pendidikan dan Pelatihan Tambang Bawah Tanah sebagai dasar untuk meningkatkan pelayanan untuk mewujudkan tata pemerintahan yang baik, diperlukan suatu dukungan penyelenggaraan kegiatan administrasi kepegawaian yang tertib dan akuntabel, sehingga dapat menunjang terselenggaranya tugas pokok dan fungsi Balai Diklat Tambang Bawah Tanah. 
kegiatan Pengelolaan Manajemen SDM dan Tata Kelola Administrasi Kepegawaian meliputi:
a. Pengelolaan Administrasi Kepegawaian
b. Penilaian Angka Kredit Pejabat Fungsional
c. Inventarisasi Arsip Balai Diklat Tambang Bawah Tanah
d. Pengelolaan Administrasi Perpustakaan
e. Penerapan Peta Jabatan PNS dan Sosialisasi Informasi Kepegawaian
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "penataan organisasi dan akreditasi lembaga",
            "deskripsi_pengetahuan" => <<<'EOD'
Sebagai salah satu upaya dalam menjaga dan secara terus menerus meningkatkan kualitas pendidikan dan pelatihan yang diberikan pada peserta diklat sebagai salah satu stakeholder, dalam hal ini masyarakat Indonesia, sebuah lembaga perlu melakukan suatu upaya untuk menjaga sistem manajemennya sehingga sasaran mutu atau visis dan misis lembaga atau organisasi dapat tercapai secara terstruktur. Untuk dapat menjaga suatu sistem manajemen diperlukan suatu standar untuk menjadi pedoman dlam pelaksanaan manajemen suatu organisasi. Salah satu standar yang populer digunakan di dunia dan telah diterapkan oleh banyak lembaga dan organisasi pemerintahan di dunia adalah ISO 9001. ISO 9001 adalah suatu standar dalam manajemen suatu organisasi agar organisasi tersebut dapat terus malakukan peningkatan kualitas mutu kerja mauput produk yang berkelanjutan. ISO 9001 yang digunakan adalah versi 2008.

Dalam menerapkan ISO 9001:2008 suatu organisasi perlu menyiapkan dokumen-dokumen yang wajib dimiliki sebuah organisasi. Diantaranya yaitu SOP organisasi, Dokumen Pedoman Mutu Organisasi, dll. Di dalam SOP Organisasi diwajibkan memenuhi klausa-klausa yang disyaratkan oleh ISO 9001 sehingga setiap kegiatan kerja yang dilakukan di dalam organisasi telah sesuai dengan apa yang disyaratkan di dalam ISO 9001:2008. Selain itu, dalam penerapannya, organisasi perlu melakukan audit dan review (tinjauan) ke dalam organisasinya sendiri dimana pempinan organisasi bertindak sebagai manajer puncak yang bertangung jawab atas pelaksanaan sistem manajemen organisasi. Dan untuk mendapat sebuah pengakuan dari peihak luar bahawa organisasi telah menerapkan sistem manajemen ISO 9001:2008, organisasi perlu meminta pihak lembaga sertifkasi (LPK) yang telah bersertifikat untuk mengaudit organisasinya sebagai pihak eksternal. Dari hasil audit inilah nanti ditetapkan oleh lembaga sertifikasi tersebut apakeh organisasi pendidikan dan pelatihan telah layak memiliki sertifikat ISO 9001:2008.

Disamping penggunaan sistem manajemen ISO 9001:2008 organisasi BDTBT juga menerapkan sistem manajemen kesehatan dan keselamatan kerja OHSAS 18001:2007. Penerapan sistem ini, selain untuk memberikan jaminan kesehatan dan keselamatan kerja bagi pegawai, dimaksudkan untuk memberikan pengalaman bagi peserta diklat agar dapat merasakan lingkungan kerja seperti lingkungan kerja yang diterpkan di pekerjaan bidang tambang bawah tanah dimana penerapan sistem kesehatan dan keselamatan kerja merupakan hal yang esensial untuk dilakukan oleh perusahaan tambang bawah tanah. Dalam penerapannya, seperti halnya ISO 9001:2008, OHSAS 18001:2007 juga mensyaratkan organisasi memiliki dokumen-dokumen K-3 dan melakukan audit dan review organisasi.

Selain dua hal diatas, dengan adanya laboratorium analisis batubara di BDTBT maka, sesuai dengan amanan Menteri ESDM tentang pemanfaatan potensi lembaga untuk dapat menghasilkan pemasukan sendiri, laboratorium tersebut merupakan suatu potensi untuk menghasilkan pemasukan negara dari analisis batubara. Untuk meningkatkan daya saing laboratorium batubara BDTBT, BDTBT perlu memiliki pengakuan dari lembaga remi pemerintahan yang memiliki wewenang dalam memberikan Akreditasi organisasi, dalam hal ini Komisi Akreditasi Nasional (KAN). Satndar untuk akreditasi laboraorium yang digunakan oleh KAN adalah ISO 17025:2005 sehingga laboratorium BDTBT perlu menerapkan standar tersebut untuk mendapatkan Akreditasi dari KAN. Sama halnya dengan ISO 9001:2008 dan OHSAS 18001:2007, Laboratorium juga diwajibkan memiliki dokumen-dokumen tersendiri serta melakukan audit dan review terhadap pelaksanaan sistem. Begitu juga dengan pengauditan dari pihak luar yang dalam hal ini dilakukan oleh KAN.

Akreditasi KAN memberikan sejumlah manfaat penting untuk lembaga penilaian kesesuaian yang meningkatkan kepercayaan diri dalam penilaian kesesuaian.

a. Kompetensi
Dengan menyesuaikan  kebijakan dan prosedur KAN yang telah diakui internasional secara teratur dengan fokus terhadap peningkatan kompetensi dalam melakukan kegiatan penilaian kesesuaian.

b. Menunjukan kemandirian 
Untuk penilaian yang efektif oleh Lembaga Penilaian Kesesuaian sangat penting independensi dalam pelaksanaannya. Akreditasi KAN menekankan independensi LPK Dalam melaksanakan penilaian.

c. Memperoleh pengakuan internasional
KAN telah diakui secara formal oleh  sejumlah organisasi akreditasi intemational termasuk International Accreditation Forum (IAF), Pacific Accreditation Cooperation (PAC}. Asia Pacific Laboratory Accreditation Cooperation (APLAC), and Intemational Laboratory Accreditation Cooperation (ILAC).
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Penjalinan Kerjasama Diklat Dalam Negeri dan Luar Negeri",
            "deskripsi_pengetahuan" => <<<'EOD'
Pelaksanaan peningkatan  kompetensi sumber daya manusia perlu di dukung oleh ketersediaan tenaga pendidik yang berkualitas, jumlah dan mutu peralatan pendukung, serta dana yang memadai. Perluasan jejaring kerjasama sangat penting untuk meningkatkan kualitas suatu organisasi, karena dengan adanya jejaring kerjasama kebutuhan organisasi dapat dengan segera dipenuhi, baik dengan cara pinjam-pakai, bantuan (grant) maupun pinjaman lunak.
Kerjasama tersebut diharapkan dapat meningkatkan kompetensi sumber daya manusia dibidang teknologi mineral dan batubara khususnya tambang bawah tanah. Untuk dapat menjalin kerjsama dengan institusi lain di luar kementerian ESDM, diperlukan kegiatan penjajagan kerjasama yang intensif dan efektif. Sehingga apabila nantinya kerjasama ini dapat terwujud dalam suatu dokumen Mou, kerjasama ini akan dapat membawa manfaat bagi kedua belah pihak. Dan juga pelaksanaan kerjasama ini akan benar-benar berjalan sesuai yang diharapkan.            
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Evaluasi Pelaksanaan Implementasi Kerjasama Dalam Negeri dan Luar Negeri",
            "deskripsi_pengetahuan" => <<<'EOD'
Evaluasi Kerjasama adalah bagian dari hasil dari implementasi dari kegiatan kerjasama dengan institusi lain yang diterapkan kedalam suatu hasil yang telah terlaksana. Sejak berdirinya Balai Pendidikan dan Pelatihan Tambang bawah tanah tahun 2002 banyak sekali perubahan-perubahan dari segi struktural maupun organisasi untuk mengikuti perkembangan yang ada. Dalam hal mendukung suatu organisasi yang nota bene belum begitu lama mempunyai sejarah perkembangannya maka Balai Pendidikan dan Pelatihan Tambang Bawah Tanah dalam meningkatkan kelembagaan. Baik SDM maupun organisasinya dari hal tersebut maka harus meningkatkan penjalinan Kerjasam dengan institusi lain. 
Penjalinan kerjasama dengan institusi lain sangat berpengaruh bagi kemajuan Unit Balai pendidikan dan Pelatihan tambang bawah tanah karena untuk mempromosikan unit organisasi salah satunya yaitu dengan meningkatkan kerjasama antar institusi ataupun lembaga baik dengan pemerintah(G2G), dengan swasta (G2B), dengan Masyarakat (G2C), dan pemerintah dengan SDMnya (G2E).
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Pengembangan, Pembangunan, dan Implementasi TIK (Teknologi Informasi dan Komunikasi)",
            "deskripsi_pengetahuan" => <<<'EOD'
Pemerintahan elektronik atau e-government (berasal dari kata Bahasa Inggris electronics government, juga disebut e-gov, digital government, online government atau dalam konteks tertentu transformational government) adalah penggunaan teknologi informasi oleh pemerintah untuk memberikan informasi dan pelayanan bagi warganya, urusan bisnis, serta hal-hal lain yang berkenaan dengan pemerintahan. e-Government dapat diaplikasikan pada legislatif, yudikatif, atau administrasi publik, untuk meningkatkan efisiensi internal, menyampaikan pelayanan publik, atau proses kepemerintahan yang demokratis. Model penyampaian yang utama adalah Government-to-Citizen atau Government-to-Customer (G2C), Government-to-Business (G2B) serta Government-to-Government (G2G). Keuntungan yang paling diharapkan dari e-government adalah peningkatan efisiensi, kenyamanan, serta aksesibilitas yang lebih baik dari pelayanan publik.

Perkembangan Teknologi informasi yang semakin pesat, baik dalam segi perangkat keras maupun perangkat lunak berdampak pada pola pengelolaan data dan informasi yang harus semakin cepat, akurat dan terkini (uptodate). Penerapan e-government di lingkungan Balai Diklat Tambang Bawah Tanah merupakan upaya untuk mengembangkan penyelenggaraan kepemerintahan yang berbasis (menggunakan) elektronik dalam rangka meningkatkan kualitas layanan publik secara efektif dan efisien. Melalui penerapan e-government dilakukan penataan sistem manajemen dan proses kerja di lingkungan Balai Diklat Tambang Bawah Tanah dengan mengoptimasikan pemanfaatan teknologi informasi. Pemanfaatan teknologi informasi tersebut mencakup 2 (dua) aktivitas yang berkaitan yaitu :

a. Pengembangan dan Pengelolaan Website
Website merupakan salah satu sarana pendukung dari kegiatan Balai Pendidikan dan Pelatihan Tambang Bawah Tanah yang digunakan memperlancar proses publikasi dan sosialisai pemberian  informasi kepada masyarakat perihal pendidikan dan pelatihan tambang bawah tanah. Program Kegiatan Pengelolaan website merupakan salah satu bagian dari kebijakan dan Strategi Nasional Pengembangan e-Government yang merupakan landasan menuju penyelenggaraan pemerintahan yang baik (good goverment dan meningkatkan layanan public yang efektif dan efisien.
Media informasi, komunikasi dan telekomunikasi terus semakin dituntut untuk memenuhi kebutuhan dalam waktu yang semakin cepat (kapan saja), tepat(akurat), menarik(atraktif) dan menjangkau ruang yang luas (global). Dalam hal ini internet dan lebih khusus lagi Website adalah sebagai solusi. Balai Diklat Tambang Bawah Tanah menyadari sepenuhnya hal ini. Namun saat ini pengelolaan website belum berjalan optimal. untuk itu sangat perlu ditingkatkan dan didukung kegiatan Pengembangan dan Pengelolaan website. Dengan pengelolaan yang baik diyakini akan sangat memfasilitasi pelayanan informasi, komunikasi yang sangat dibutuhkan dalam memberikan pelayanan pendidikan dan pelatihan Tambang Bawah Tanah.

b. Pengembangan dan Pengelolaan Infrastruktur LAN dan Jaringan Komunikasi
Terbentuknya kepemerintahan yang bersih, transparan, dan mampu menjawab tuntutan perubahan secara efektif. Menuntut perubahan-perubahan yang ada, Penggunaan Teknologi informasi pada Balai Pendidikan dan Pelatihan Tambang Bawah Tanah telah memenuhi kriteria yang diperlukan. Sebagai penyelenggara pendidikan dan pelatihan, pengelolaan Teknologi Informasi Sangat diperlukan salah satunya Pengelolaan Local Area Network (LAN) atau Jaringan Komunikasi, dengan didukung kegiatan penyusunan Pengembangan Infrastruktur LAN dan Jaringan Komunikasi , hal tersebut sangat diperluan untuk meningkatkan Tata kerja Organisasi. Sarana pendukung informasi yang ada saat ini menuntut untuk dipelihara atau di kelola, Karena merupakan salah satu sarana pendukung dari kegiatan Balai Pendidikan dan Pelatihan Tambang Bawah Tanah yang digunakan memperlancar proses publikasi dan sosialisai pemberian  informasi kepada masyarakat perihal pendidikan dan pelatihan tambang bawah tanah. 

Sistem informasi berbasis komputer memerlukan perawatan baik untuk hardware(perangkat keras) maupun software(perangkat lunak). perawatan perangkat Keras Salah Satunya LAN dan Jaringan Komunikasi harus dilakukan secara berkesinambungan sehingga sistem yang ada dapat berjalan dengan baik. Maksud dan tujuan Pengembangan dan Pengelolaan infrastruktur LAN dan Jaringan Komunikasi adalah :
o Untuk memperlancar komunikasi antar unit Sektor ESDM
o komponen Perangkat keras sangat membutuhkan pemeliharaan secara baik dan teratur agar tidak terjadi kerusakan yang signifikan.
o Sistem Keamanan Inforrmasi yang terpelihara.
EOD
        ],
        [ 
            "nama_pengetahuan" =>  "Publikasi dan Promosi Kediklatan",
            "deskripsi_pengetahuan" => <<<'EOD'
Indonesia merupakan sebuah Negara yang memiliki begitu banyak sumber daya alam potensial. Salah satunya sumber daya alam yang dimiliki adalah batubara yang merupakan sumber energy alternatif, sebagai salah satu produk pertambangan umum yang strategis, saat ini batubara mempunyai peranan yang sangat penting yaitu sebagai komoditas ekspor, selain itu juga sebagai pemasok utama kebutuhan energi dalam negeri sehingga dapat menjamin pembangunan nasional secara berkesinambungan, yang pada akhirnya diharapkan dapat mendukung program peningkatan perekonomian Indonesia sehingga tercapai kesejahteraan rakyat Indonesia. Industri tambang di Indonesia dalam operasinya melakukan metoda penambangan dengan dua cara yaitu tambang terbuka dan tambang bawah tanah. Namun, hingga saat ini metode penambangan terbuka masih mendominasi dunia pertambangan di Indonesia. Seiring dengan akan semakin menipisnya cadangan bahan galian dangkal dimasa yang akan datang dan meningkatnya kesadaran masyarakat akan kelestarian lingkungan dimasa yang akan datang metode penambangan bawah tanah akan menjadi alternative pilihan utama. Untuk itu, teknologi penambangan bawah tanah harus selalu dikembangkan agar semakin maju. 

Balai Diklat Tambang Bawah Tanah dengan dukungan pengalaman dan profesionalitas serta kerjasama internasional telah dan sedang mengembangkan serta menyusun program-program pendidikan dan pelatihan guna mendukung program pemerintah dalam menyiapkan, mendidik dan melatih tenaga-tenaga siap pakai di industry pertambangan yang memiliki kompetensi yang baik di bidang pertambangan bawah tanah. Keberadaan Balai Diklat Tambang Bawah Tanah sebagai lembaga pendidikan dan pelatihan pemerintah bidang tambang bawah tanah yang baru berdiri tahun 2001, perlu kiranya disosialisasikan dan dipromosikan agar lebih banyak masyarakat dan stake holder yang mengetahui serta dapat memanfaatkannya.

Untuk itu, kegiatan Pameran / Visualisasi / Publikasi dan Promosi Balai Diklat Tambang Bawah Tanah sangat perlu dilaksanakan setiap tahunnya. Kegiatan publikasi dan promosi organisasi balai pedidikan dan pelatihan tambang bawah tanah antara lain sebagai berikut : 

a. Sosialisasi Bidang Pertambangan Bawah Tanah
Melalui kegiatan sosialisasi ini, tim Balai Diklat Tambang Bawah Tanah mendatangi langsung  instansi-instansi di daerah yang potensial sebagai pengguna diklat bidang tambang bawah tanah. Diharapkan melalui kegiatan ini didapat juga masukan dari daerah agar Balai Diklat Tambang semakin baik dan sesuai dengan harapan stake holder.

b. Penyertaan Pameran
Salah satu kegiatan yang sangat efektif untuk memperkenalkan suatu institusi adalah dengan keikutsertaan pada pameran-pameran. Melalui ajang pameran biasanya akan banyak datang orang-orang dari berbagai latar belakang yang berbeda. Diharapkan melalui pameran ini lebih banyak lagi orang dari latar belakang yang berbeda akan mengenal Balai Diklat Tambang Bawah Tanah
EOD
        ],        
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $d) {
            factory(App\Models\PengetahuanOrganisasi::class)->create([
                'nama_pengetahuan' => $d['nama_pengetahuan'],
                'deskripsi_pengetahuan' => $d['deskripsi_pengetahuan'],
            ]);
        }
    }
}
