<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\PengetahuanOrganisasi::class, function (Faker $faker) {
    return [
        'nama_pengetahuan' => $faker->sentence(),
        'deskripsi_pengetahuan' => $faker->sentence(),
        'upload_dokumen' => file_get_contents(base_path('database/seeds/pdf-sample.pdf')),
        'tanggal' => $faker->dateTime(),
        'created_by' => 'admin',
    ];
});
