<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Kasus::class, function (Faker $faker) {
    return [
        'kasus' => $faker->sentence(),
        'kata_kunci' => $faker->words(3, true),
        'indikator' => $faker->sentence(),
        'solusi' => $faker->paragraph(),
        'alat' => $faker->sentence(),
        'upload_foto' => file_get_contents(base_path('database/seeds/photo-sample.png')),
        'upload_dokumen' => file_get_contents(base_path('database/seeds/pdf-sample.pdf')),
        'upload_video' => file_get_contents(base_path('database/seeds/video-sample.mp4')),
        'upload_audio' => file_get_contents(base_path('database/seeds/audio-sample.mp3')),
        'feedback'=> $faker->paragraph(),
        'rate' => $faker->numberBetween(1, 5),
    ];
});
