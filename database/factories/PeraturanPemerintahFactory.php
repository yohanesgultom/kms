<?php

use Faker\Generator as Faker;


$factory->define(App\Models\PeraturanPemerintah::class, function (Faker $faker) {
    return [
        'jenis' => $faker->randomElement(array_keys(App\Models\PeraturanPemerintah::JENIS)),
        'perihal' => $faker->sentence(),
        'upload_dokumen' => file_get_contents(base_path('database/seeds/pdf-sample.pdf')),
        'tanggal' => $faker->dateTime(),
        'created_by' => 'admin'
    ];
});
