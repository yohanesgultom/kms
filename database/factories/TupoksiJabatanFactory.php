<?php

use Faker\Generator as Faker;

$factory->define(App\Models\TupoksiJabatan::class, function (Faker $faker) {
    return [
        'nama_jabatan' => $faker->jobTitle,
        'ikhtisar_jabatan' => $faker->sentence(),
        'uraian_tugas' => $faker->sentence(),
        'hasil_kerja' => $faker->sentence(),
        'tanggung_jawab' => $faker->sentence(),
        'syarat_jabatan' => $faker->sentence(),
        'tanggal' => $faker->dateTime(),
        'upload_dokumen' => file_get_contents(base_path('database/seeds/pdf-sample.pdf'))
    ];
});
