<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Pegawai::class, function (Faker $faker) {
    $user = factory(App\User::class)->states('pegawai')->create();
    return [
        'NIP_NIK' => $faker->isbn13(),
        'seksi_subbagian' => $faker->jobTitle(),
        'tmt_jabatan' => $faker->sentence(),
        'user_id' => $user->id,        
    ];
});
