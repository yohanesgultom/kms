<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Expert::class, function (Faker $faker) {
    return [
        'available_date' => $faker->dateTimeInInterval('+ 3 days', '+ 1 years'),
        'keahlian' => $faker->jobTitle()
    ];
});
