<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $name = $faker->name;
    $username = str_replace('-', '.', str_slug($name));
    return [
        'name' => $name,
        'username' => $username,
        'email' => $faker->unique()->safeEmail,
        'no_telp' => $faker->e164PhoneNumber,
        'password' => $username,
        'remember_token' => str_random(10),
    ];
});


$factory->state(App\User::class, 'expert', [
    'role' => App\User::ROLES['EXPERT'],
]);


$factory->state(App\User::class, 'pegawai', [
    'role' => App\User::ROLES['PEGAWAI'],
]);
