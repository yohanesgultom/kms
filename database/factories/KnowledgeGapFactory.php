<?php

use Faker\Generator as Faker;

$factory->define(App\Models\KnowledgeGap::class, function (Faker $faker) {
    return [
        'kepentingan' => $faker->numberBetween(1, 5),
        'pemahaman' => $faker->numberBetween(1, 5),
    ];
});
