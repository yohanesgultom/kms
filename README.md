# Knowledge Management System

Knowledge Management System (KMS) untuk Balai Diklat Tambang Bawah Tanah (BDTPT) Kementerian ESDM Indonesia.

## Software Dependency

Untuk dijalankan, KMS ini membutuhkan:

* PHP >= 7.x
* MySQL >= 5.6.8

Untuk pengembangan, dibutuhkan juga:

* Composer 1.5.2 https://getcomposer.org/
* Laravel 5.5 https://laravel.com/

