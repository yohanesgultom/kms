#!/bin/bash

dbname="kms"
dbuser="root"
dbpass="root"

# clean up
php artisan view:clear
php artisan cache:clear
rm app-shared.zip && rm public-shared.zip && rm database.sql && rm storage/logs/laravel.log

# recreate database to reset auto-increment indexes
# mysqladmin -u"$dbuser" -p"$dbpass" --force=TRUE drop "$dbname"
# mysqladmin -u"$dbuser" -p"$dbpass" --force=TRUE create "$dbname"

# migrate and seed db
# php artisan migrate:refresh --seed

# export sql
# mysqldump -u"$dbuser" -p"$dbpass" "$dbname" -r database.sql

# zip app and public directories
zip -r app-shared.zip . -x .\* -x \*.zip -x \*.xml -x \*.sql -x \*.sh -x .git/\* -x public/\* -x database/\* -x tests/\* -x node_modules/\* -x webpack.mix.js -x package.json
cd public; zip -r ../public-shared.zip . -x index.php -x .htaccess -x \*.json;cd ..
